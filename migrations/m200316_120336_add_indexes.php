<?php

use yii\db\Migration;

/**
 * Class m200316_120336_add_indexes
 */
class m200316_120336_add_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createIndex('idx-status', 'call', 'status');
		$this->createIndex('idx-direction', 'call', 'direction');
		$this->createIndex('idx-phone_from', 'call', 'phone_from');
		$this->createIndex('idx-phone_to', 'call', 'phone_to');

		$this->createIndex('idx-status', 'customer', 'status');

		$this->createIndex('idx-direction', 'fax', 'direction');
		$this->createIndex('idx-type', 'fax', 'type');
		$this->createIndex('idx-from', 'fax', 'from');
		$this->createIndex('idx-to', 'fax', 'to');

		$this->createIndex('idx-event', 'history', 'event');
		$this->createIndex('idx-object', 'history', 'object');
		$this->createIndex('idx-object_id', 'history', 'object_id');

		$this->alterColumn('sms', 'direction', $this->smallInteger());
		$this->createIndex('idx-direction', 'sms', 'direction');
		$this->createIndex('idx-status', 'sms', 'status');
		$this->createIndex('idx-phone_from', 'sms', 'phone_from');
		$this->createIndex('idx-phone_to', 'sms', 'phone_to');

		$this->createIndex('idx-status', 'task', 'status');
		$this->createIndex('idx-priority', 'task', 'priority');

		$this->createIndex('idx-status', 'user', 'status');
	}

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200316_120336_add_indexes cannot be reverted.\n";

		$this->dropIndex('idx-status', 'call');
		$this->dropIndex('idx-direction', 'call');
		$this->dropIndex('idx-phone_from', 'call');
		$this->dropIndex('idx-phone_to', 'call');

		$this->dropIndex('idx-status', 'customer');

		$this->dropIndex('idx-direction', 'fax');
		$this->dropIndex('idx-type', 'fax');
		$this->dropIndex('idx-from', 'fax');
		$this->dropIndex('idx-to', 'fax');

		$this->dropIndex('idx-event', 'history');
		$this->dropIndex('idx-object', 'history');
		$this->dropIndex('idx-object_id', 'history');

		$this->alterColumn('sms', 'direction', $this->tinyInteger(3));
		$this->dropIndex('idx-direction', 'sms');
		$this->dropIndex('idx-status', 'sms');
		$this->dropIndex('idx-phone_from', 'sms');
		$this->dropIndex('idx-phone_to', 'sms');

		$this->dropIndex('idx-status', 'task');
		$this->dropIndex('idx-priority', 'task');

		$this->dropIndex('idx-status', 'user');


        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200316_120336_add_indexes cannot be reverted.\n";

        return false;
    }
    */
}
