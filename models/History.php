<?php

namespace app\models;


use app\components\historydetail\HistoryDetail;
use app\components\historydetail\HistoryDetailInterface;
use app\models\interfaces\EventInterface;
use app\models\interfaces\HistoryInterface;
use app\models\interfaces\UserInterface;
use app\models\traits\ObjectNameTrait;
use Yii;


/**
 * This is the model class for table "{{%history}}".
 *
 * @property integer $id
 * @property string $ins_ts
 * @property integer $customer_id
 * @property string $event
 * @property string $object
 * @property integer $object_id
 * @property string $message
 * @property string $detail
 * @property integer $user_id
 *
 * @property Customer $customer
 * @property User $user
 */
class History extends \yii\db\ActiveRecord implements HistoryInterface
{
    use ObjectNameTrait;

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ins_ts'], 'safe'],
            [['customer_id', 'object_id', 'user_id'], 'integer'],
            [['event'], 'required'],
            [['message', 'detail'], 'string'],
            [['event', 'object'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ins_ts' => Yii::t('app', 'Ins Ts'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'event' => Yii::t('app', 'Event'),
            'object' => Yii::t('app', 'Object'),
            'object_id' => Yii::t('app', 'Object ID'),
            'message' => Yii::t('app', 'Message'),
            'detail' => Yii::t('app', 'Detail'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    protected function getRelationClassMap(): array
	{
		return [
			Customer::SHORT_CLASSNAME => Customer::class,
			Sms::SHORT_CLASSNAME => Sms::class,
			Task::SHORT_CLASSNAME => Task::class,
			Call::SHORT_CLASSNAME => Call::class,
			Fax::SHORT_CLASSNAME => Fax::class,
		];
	}

	public function getEventType(): string
	{
		return $this->event;
	}

	public function getRelationObject(): ?EventInterface
	{
		try {
			return $this->{$this->object};
		} catch (\Exception $e) {
			return null;
		}
	}

	/**
	 * @return UserInterface|null
	 */
	public function getSender(): ?UserInterface
	{
		return $this->user;
	}

	/**
	 * @return string
	 */
	public function getDate(): string
	{
		return $this->ins_ts;
	}

	/**
	 * @return HistoryDetailInterface|null
	 */
	public function getDetail(): ?HistoryDetailInterface
	{
		if(!$this->detail) {
			return null;
		}

		return new HistoryDetail($this->detail);
	}
}
