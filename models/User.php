<?php

namespace app\models;

use app\components\status\interfaces\UserStatusInterface;
use app\components\status\UserStatus;
use app\components\translation\Translation;
use app\models\interfaces\UserInterface;
use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord implements UserInterface
{
	const SHORT_CLASSNAME = 'user';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'created_at', 'updated_at'], 'required'],
            [[
                'status',
                'created_at',
                'updated_at',
            ], 'integer'],
            [[
                'username',
                'email',
            ], 'string', 'max' => 255],

            [['username'], 'unique'],

            ['status', 'default', 'value' => UserStatus::STATUS_ACTIVE],
            ['status', 'in', 'range' => UserStatus::getList()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username (login)'),
            'statusText' => Yii::t('app', 'Status'),
        ];
    }

	/**
	 * @return UserStatusInterface
	 */
    public function getStatus(): UserStatusInterface
	{
		return new UserStatus($this->status, new Translation());
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->username;
	}
}
