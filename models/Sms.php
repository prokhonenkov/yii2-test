<?php

namespace app\models;

use app\components\direction\Direction;
use app\components\direction\DirectionInterface;
use app\components\status\interfaces\SmsStatusInterface;
use app\components\status\SmsStatus;
use app\components\translation\Translation;
use app\models\interfaces\SmsInterface;
use app\models\traits\EventTrait;
use Yii;

/**
 * This is the model class for table "{{%sms}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $customer_id
 * @property integer $status
 * @property string $phone_from
 * @property string $message
 * @property string $ins_ts
 * @property integer $direction
 * @property string $phone_to
 * @property integer $type
 * @property string $formatted_message
 *
 * @property string $statusText
 * @property string $directionText
 *
 * @property Customer $customer
 * @property User $user
 */
class Sms extends \yii\db\ActiveRecord implements SmsInterface
{
	use EventTrait;

	const SHORT_CLASSNAME = 'sms';

	public const EVENT_INCOMING = 'incoming_sms';
	public const EVENT_OUTGOING = 'outgoing_sms';

	public static array $eventMap = [
		self::EVENT_INCOMING => 'Incoming',
		self::EVENT_OUTGOING => 'Outgoing',
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone_to', 'direction'], 'required'],
            [['user_id', 'customer_id', 'status', 'direction', 'applicant_id', 'type'], 'integer'],
            [['message'], 'string'],
            [['ins_ts'], 'safe'],
            [['phone_from', 'phone_to'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'status' => Yii::t('app', 'Status'),
            'statusText' => Yii::t('app', 'Status'),
            'phone_from' => Yii::t('app', 'Phone From'),
            'phone_to' => Yii::t('app', 'Phone To'),
            'message' => Yii::t('app', 'Message'),
            'ins_ts' => Yii::t('app', 'Date'),
            'direction' => Yii::t('app', 'Direction'),
            'directionText' => Yii::t('app', 'Direction'),
            'user.fullname' => Yii::t('app', 'User'),
            'customer.name' => Yii::t('app', 'Client'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	/**
	 * @return SmsStatusInterface
	 */
    public function getStatus(): SmsStatusInterface
	{
		return new SmsStatus($this->status, new Translation());
	}

	/**
	 * @return DirectionInterface
	 */
	public function getDirection(): DirectionInterface
	{
		return new Direction($this->direction, [
			Direction::DIRECTION_INCOMING => Yii::t('app', 'Incoming'),
			Direction::DIRECTION_OUTGOING => Yii::t('app', 'Outgoing'),
		]);
	}

	/**
	 * @return string
	 */
	public function getPhoneFrom(): string
	{
		return $this->phone_from;
	}

	/**
	 * @return string
	 */
	public function getPhoneTo(): string
	{
		return $this->phone_to;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}
}
