<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 9:49
 */

namespace app\models\interfaces;


interface CustomerInterface extends EventInterface
{
	/**
	 * @return string
	 */
	public function getName(): string ;
}