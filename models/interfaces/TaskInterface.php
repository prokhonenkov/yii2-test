<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 9:53
 */

namespace app\models\interfaces;


use app\components\status\interfaces\TaskStatusInterface;

interface TaskInterface extends EventInterface
{
	/**
	 * @return CustomerInterface
	 */
	public function getCreditor(): CustomerInterface ;

	/**
	 * @return string
	 */
	public function getTitle(): string ;

	/**
	 * @return TaskStatusInterface
	 */
	public function getStatus(): TaskStatusInterface;
}