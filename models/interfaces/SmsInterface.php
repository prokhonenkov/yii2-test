<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 10:11
 */

namespace app\models\interfaces;


use app\components\direction\DirectionInterface;
use app\components\status\interfaces\SmsStatusInterface;

interface SmsInterface extends EventInterface
{
	/**
	 * @return DirectionInterface
	 */
	public function getDirection(): DirectionInterface ;

	/**
	 * @return string
	 */
	public function getPhoneFrom(): string ;

	/**
	 * @return string
	 */
	public function getPhoneTo(): string ;

	/**
	 * @return string
	 */
	public function getMessage(): string ;

	/**
	 * @return SmsStatusInterface
	 */
	public function getStatus(): SmsStatusInterface ;
}