<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 12:33
 */

namespace app\models\interfaces;


use app\components\direction\DirectionInterface;
use app\components\status\interfaces\CallStatusInterface;

interface CallInterface extends EventInterface
{
	/**
	 * @return string|null
	 */
	public function getComment(): ?string ;

	/**
	 * @return DirectionInterface
	 */
	public function getDirection(): DirectionInterface ;

	/**
	 * @return string
	 */
	public function getInfo(): string ;

	/**
	 * @return CallStatusInterface
	 */
	public function getStatus(): CallStatusInterface ;
}