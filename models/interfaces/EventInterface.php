<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 22.03.2020
 * Time 8:41
 */

namespace app\models\interfaces;


interface EventInterface
{
	/**
	 * @return string
	 */
	public static function getEventTitleByName(string $name): string ;
}