<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 12:12
 */

namespace app\models\interfaces;


use app\components\historydetail\HistoryDetailInterface;

interface HistoryInterface
{
	public function getEventType(): string ;

	public function getRelationObject(): ?EventInterface ;

	public function getSender(): ?UserInterface ;

	public function getDetail(): ?HistoryDetailInterface ;

	public function getDate(): string ;
}