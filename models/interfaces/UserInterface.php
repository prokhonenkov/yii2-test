<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 15:11
 */

namespace app\models\interfaces;


use app\components\status\interfaces\UserStatusInterface;

interface UserInterface
{
	/**
	 * @return string
	 */
	public function getName(): string ;

	/**
	 * @return UserStatusInterface
	 */
	public function getStatus(): UserStatusInterface;
}