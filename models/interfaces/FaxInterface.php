<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 11:52
 */

namespace app\models\interfaces;


use app\components\faxtype\FaxTypeInterface;

interface FaxInterface extends EventInterface
{
	/**
	 * @return FaxTypeInterface
	 */
	public function getType(): ?FaxTypeInterface ;
}