<?php

namespace app\models;

use app\components\status\interfaces\TaskStatusInterface;
use app\components\status\TaskStatus;
use app\components\translation\Translation;
use app\models\interfaces\CustomerInterface;
use app\models\interfaces\TaskInterface;
use app\models\traits\EventTrait;
use Yii;

/**
 * This is the model class for table "{{%task}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $customer_id
 * @property integer $status
 * @property string $title
 * @property string $text
 * @property string $due_date
 * @property integer $priority
 * @property string $ins_ts
 *
 * @property Customer $customer
 * @property User $user
 *
 */
class Task extends \yii\db\ActiveRecord implements TaskInterface
{
	use EventTrait;

	const SHORT_CLASSNAME = 'task';

	public const EVENT_CREATED = 'created_task';
	public const EVENT_UPDATED = 'updated_task';
	public const EVENT_COMPLETED = 'completed_task';

	public static array $eventMap = [
		self::EVENT_CREATED => 'Created',
		self::EVENT_UPDATED => 'Updated',
		self::EVENT_COMPLETED => 'Completed',
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title'], 'required'],
            [['user_id', 'customer_id', 'status', 'priority'], 'integer'],
            [['text'], 'string'],
            [['title', 'object'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'status' => Yii::t('app', 'Status'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Description'),
            'due_date' => Yii::t('app', 'Due Date'),
            'formatted_due_date' => Yii::t('app', 'Due Date'),
            'priority' => Yii::t('app', 'Priority'),
            'ins_ts' => Yii::t('app', 'Ins Ts'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	/**
	 * @return TaskStatusInterface
	 */
    public function getStatus(): TaskStatusInterface
	{
		return new TaskStatus($this->status, $this->due_date, new Translation());
	}

	/**
	 * @return string
	 */
    public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return CustomerInterface
	 */
	public function getCreditor(): CustomerInterface
	{
		return $this->customer;
	}
}
