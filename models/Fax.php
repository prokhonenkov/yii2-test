<?php

namespace app\models;

use app\components\faxtype\FaxType;
use app\components\faxtype\FaxTypeInterface;
use app\components\translation\Translation;
use app\models\interfaces\FaxInterface;
use app\models\traits\EventTrait;
use Yii;

/**
 * This is the model class for table "fax".
 *
 * @property integer $id
 * @property string $ins_ts
 * @property integer $user_id
 * @property string $from
 * @property string $to
 * @property integer $status
 * @property integer $direction
 * @property integer $type
 * @property string $typeText
 *
 * @property User $user
 */
class Fax extends \yii\db\ActiveRecord implements FaxInterface
{
	use EventTrait;

	const SHORT_CLASSNAME = 'fax';

	public const EVENT_INCOMING = 'incoming_fax';
	public const EVENT_OUTGOING = 'outgoing_fax';

	public static array $eventMap = [
		self::EVENT_INCOMING => 'Incoming',
		self::EVENT_OUTGOING => 'Outgoing',
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fax';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['ins_ts'], 'safe'],
            [['user_id'], 'integer'],
            [['from', 'to'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ins_ts' => Yii::t('app', 'Created Time'),
            'user_id' => Yii::t('app', 'User ID'),
            'from' => Yii::t('app', 'From'),
            'to' => Yii::t('app', 'To')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

	/**
	 * @return FaxTypeInterface
	 */
    public function getType(): ?FaxTypeInterface
	{
		if(!$this->type) {
			return null;
		}

		return new FaxType($this->type, new Translation());
	}
}
