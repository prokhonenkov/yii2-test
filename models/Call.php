<?php

namespace app\models;

use app\components\direction\Direction;
use app\components\direction\DirectionInterface;
use app\components\status\CallStatus;
use app\components\status\interfaces\CallStatusInterface;
use app\components\translation\Translation;
use app\models\interfaces\CallInterface;
use app\models\traits\EventTrait;
use Yii;

/**
 * This is the model class for table "{{%call}}".
 *
 * @property integer $id
 * @property string $ins_ts
 * @property integer $direction
 * @property integer $user_id
 * @property integer $customer_id
 * @property integer $status
 * @property string $phone_from
 * @property string $phone_to
 * @property string $comment
 *
 * -- magic properties
 * @property string $statusText
 * @property string $directionText
 * @property string $totalStatusText
 * @property string $totalDisposition
 * @property string $durationText
 *
 * @property Customer $customer
 * @property User $user
 */
class Call extends \yii\db\ActiveRecord implements CallInterface
{
	use EventTrait;

	const SHORT_CLASSNAME = 'call';

	public const EVENT_INCOMING = 'incoming_call';
	public const EVENT_OUTGOING = 'outgoing_call';

	public static array $eventMap = [
		self::EVENT_INCOMING => 'Incoming',
		self::EVENT_OUTGOING => 'OutGoing'
	];

    public $duration = 720;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ins_ts'], 'safe'],
            [['direction', 'phone_from', 'phone_to', 'type', 'status', 'viewed'], 'required'],
            [['direction', 'user_id', 'customer_id', 'type', 'status'], 'integer'],
            [['phone_from', 'phone_to', 'outcome'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ins_ts' => Yii::t('app', 'Date'),
            'direction' => Yii::t('app', 'Direction'),
            'directionText' => Yii::t('app', 'Direction'),
            'user_id' => Yii::t('app', 'User ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'status' => Yii::t('app', 'Status'),
            'statusText' => Yii::t('app', 'Status'),
            'phone_from' => Yii::t('app', 'Caller Phone'),
            'phone_to' => Yii::t('app', 'Dialed Phone'),
            'user.fullname' => Yii::t('app', 'User'),
            'customer.name' => Yii::t('app', 'Client'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	/**
	 * @return string
	 */
    public function getInfo(): string
    {
        if (!$this->getStatus()->isAnswered() && $this->getDirection()->isIncoming()) {
            return Yii::t('app', 'Missed Call');
        }

        if (!$this->getStatus()->isAnswered() && $this->getDirection()->isOutgoing()) {
            return Yii::t('app', 'Client No Answer');
        }

        $msg = $this->getDirection()->getName();

        if ($this->duration) {
            $msg .= ' (' . $this->getDurationText() . ')';
        }

        return $msg;
    }

    /**
     * @return string
     */
    public function getDurationText()
    {
        if (is_null($this->duration)) {
			return '00:00';
		}

		return $this->duration >= 3600
			? gmdate("H:i:s", $this->duration)
			: gmdate("i:s", $this->duration);
    }

	/**
	 * @return string
	 */
	public function getComment(): ?string
	{
		return $this->comment;
	}

	/**
	 * @return DirectionInterface
	 */
	public function getDirection(): DirectionInterface
	{
		return new Direction($this->direction, [
			Direction::DIRECTION_INCOMING => Yii::t('app', 'Incoming Call'),
			Direction::DIRECTION_OUTGOING => Yii::t('app', 'Outgoing Call')
		]);
	}

	/**
	 * @return CallStatusInterface
	 */
	public function getStatus(): CallStatusInterface
	{
		return new CallStatus($this->status, new Translation());
	}
}
