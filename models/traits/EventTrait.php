<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 22.03.2020
 * Time 12:05
 */

namespace app\models\traits;


use yii\base\InvalidArgumentException;

trait EventTrait
{
	public static function getEventTitleByName(string $name): string
	{
		if(!isset(self::$eventMap[$name])) {
			throw new InvalidArgumentException('Event is not exist');
		}

		return \Yii::t('app', self::$eventMap[$name]);
	}
}