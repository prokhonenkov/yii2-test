<?php
namespace app\models\traits;

use Yii;
use yii\db\ActiveQuery;

trait ObjectNameTrait
{
	/**
	 * @return array
	 */
	abstract protected function getRelationClassMap(): array ;

	/**
	 * @param $name
	 * @param bool $throwException
	 * @return ActiveQuery|\yii\db\ActiveQueryInterface
	 */
    public function getRelation($name, $throwException = true)
    {
        $getter = 'get' . $name;
        $class = $this->getRelationClassMap()[$name] ?? null;

        if ($class && !method_exists($this, $getter)) {
            return $this->hasOne($class, ['id' => 'object_id']);
        }

        return parent::getRelation($name, $throwException);
    }

	public function __get($name)
	{
		$map = $this->getRelationClassMap();

		if(isset($map[$name])) {
			return $this->hasOne($map[$name], ['id' => 'object_id'])->one();
		}
		return parent::__get($name);
	}
}