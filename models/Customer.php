<?php

namespace app\models;


use app\models\interfaces\CustomerInterface;
use app\models\traits\EventTrait;
use Yii;


/**
 * This is the model class for table "{{%customer}}".
 *
 * @property integer $id
 * @property string $name
  */
class Customer extends \yii\db\ActiveRecord implements CustomerInterface
{
	use EventTrait;

	const SHORT_CLASSNAME = 'customer';

	public const EVENT_CHANGE_TYPE = 'customer_change_type';
	public const EVENT_CHANGE_QUALITY = 'customer_change_quality';

	public static array $eventMap = [
		self::EVENT_CHANGE_QUALITY => 'Change quality',
		self::EVENT_CHANGE_TYPE => 'Change type'
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }

	/**
	 * @return string
	 */
    public function getName(): string
	{
		return $this->name;
	}
}