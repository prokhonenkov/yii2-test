<?php

class SmsStatusTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

	protected function _before()
	{
	}

	protected function _after()
	{
	}

	// tests
	public function testGetValue()
	{
		$status = $this->make(\app\components\status\SmsStatus::class, [
			'status' => \app\components\status\SmsStatus::STATUS_ANSWERED,
		]);

		$this->assertEquals($status->getValue(), \app\components\status\SmsStatus::STATUS_ANSWERED);
	}

	public function testGetTitle()
	{
		$status = $this->construct(\app\components\status\SmsStatus::class, [
			'status' => \app\components\status\SmsStatus::STATUS_ANSWERED,
			'translation' => new \app\components\translation\Translation()
		], [
		]);


		$this->assertEquals($status->getTitle(), 'Answered');
	}

	public function testIncorrectValue()
	{
		try {
			$this->construct(\app\components\status\SmsStatus::class, [
				'status' => 1000,
				'translation' => new \app\components\translation\Translation()
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
			$this->assertEquals($e->getMessage(), 'The status is incorrect');
		}
	}
}