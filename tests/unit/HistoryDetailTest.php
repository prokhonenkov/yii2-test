<?php

use app\components\historydetail\DetailItemInterface;
use app\components\historydetail\ValueInterface;

class HistoryDetailTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $detail;
    
    protected function _before()
    {
    	$this->detail = new \app\components\historydetail\HistoryDetail(json_encode([
    		'changedAttributes' => [
				'quality' => [
					'new' => \app\components\historydetail\items\Quality::QUALITY_ACTIVE,
					'old' => \app\components\historydetail\items\Quality::QUALITY_REJECTED
				],
				'type' => [
					'old' => \app\components\historydetail\items\Type::TYPE_LEAD
				]
			]
		]));
    }

    protected function _after()
    {
    }

    // tests
    public function testGetQuality()
    {
		$quality = $this->detail->getQuality();

		$this->assertTrue($quality instanceof DetailItemInterface);

		/** @var ValueInterface $oldValue */
		$oldValue = $quality->getOldValue();

		$this->assertTrue($oldValue instanceof ValueInterface);

		$this->assertTrue($oldValue->getValue() === \app\components\historydetail\items\Quality::QUALITY_REJECTED);

		/** @var ValueInterface $newValue */
		$newValue = $quality->getNewValue();

		$this->assertTrue($newValue instanceof ValueInterface);

		$this->assertTrue($newValue->getValue() === \app\components\historydetail\items\Quality::QUALITY_ACTIVE);
    }

    public function testGetType()
    {
		$type = $this->detail->getType();

		$this->assertTrue($type instanceof DetailItemInterface);

		/** @var ValueInterface $oldValue */
		$oldValue = $type->getOldValue();

		$this->assertTrue($oldValue instanceof ValueInterface);

		$this->assertTrue($oldValue->getValue() === \app\components\historydetail\items\Type::TYPE_LEAD);

		/** @var ValueInterface $newValue */
		$newValue = $type->getNewValue();

		$this->assertTrue($newValue instanceof ValueInterface);

		$this->assertTrue($newValue->getValue() === \app\components\historydetail\items\Type::NO_SET);
    }

    public function testEmptyData()
	{
		$details = new \app\components\historydetail\HistoryDetail(json_encode([
			'changedAttributes' => [

			]
		]));

		$quality = $details->getQuality();

		$this->assertTrue(is_null($quality));

		$type = $details->getType();

		$this->assertTrue(is_null($type));
	}
}