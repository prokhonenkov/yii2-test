<?php

class ValueTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $value;
    
    protected function _before()
    {
    	$this->value = new \app\components\historydetail\Value(1, 'Test', true);
    }

    protected function _after()
    {
    }

    // tests
    public function testGetValue()
    {
		$this->assertTrue($this->value->getValue() === 1);
    }

    public function testGettitle()
    {
		$this->assertTrue($this->value->gettitle() === 'Test');
    }

    public function testIsDefault()
    {
		$this->assertTrue($this->value->isDefault());
    }
}