<?php

use app\components\historylist\HistoryListRegistry;

class HistoryMessageTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

	public function _fixtures()
	{
		return [
			'history' => [
				'class' => \app\tests\fixtures\HistoryFixture::class,
				'dataFile' => codecept_data_dir() . 'History.php'
			],
			'task' => [
				'class' => \app\tests\fixtures\TaskFixture::class,
				'dataFile' => codecept_data_dir() . 'Task.php'
			],
			'sms' => [
				'class' => \app\tests\fixtures\SmsFixture::class,
				'dataFile' => codecept_data_dir() . 'Sms.php'
			],
			'call' => [
				'class' => \app\tests\fixtures\CallFixture::class,
				'dataFile' => codecept_data_dir() . 'Call.php'
			],
			'fax' => [
				'class' => \app\tests\fixtures\FaxFixture::class,
				'dataFile' => codecept_data_dir() . 'Fax.php'
			],
			'user' => [
				'class' => \app\tests\fixtures\UserFixture::class,
				'dataFile' => codecept_data_dir() . 'User.php'
			],
		];
	}

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testCreateMessage()
    {
		$history = $this->tester->grabFixture('history', 'task');

		/** @var \app\components\historylist\message\HistoryMessage $message */
		$message = $this->make(\app\components\historylist\message\HistoryMessage::class, [
			'model' => $history,
			'messageTemplates' => HistoryListRegistry::getInstance()->getHistoryMessageTemplatesMap()
		]);

		$this->assertTrue(is_string($message->getText()));
		$this->assertTrue(is_string($message->getDate()));

		$this->assertTrue(is_string($message->getFooterText()));
		$this->assertTrue(is_null($message->getComment()));
		$this->assertTrue(is_string($message->getIconClass()));
		$this->assertEquals(
			$message->getMessageType(),
			\app\components\historylist\message\HistoryMessage::MESSAGE_TEMPLATE_COMMON
		);

		$this->assertEquals(
			$message->getBodyType(),
			\app\components\historylist\message\HistoryMessage::BODY_TEMPLATE_WITH_DATE
		);

		$this->assertEquals(
			$message->getFooterType(),
			\app\components\historylist\message\HistoryMessage::FOOTER_TEMPLATE_WITHOUT_DATE
		);
    }
}