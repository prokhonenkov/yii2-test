<?php

class UserStatusTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

	protected function _before()
	{
	}

	protected function _after()
	{
	}

	// tests
	public function testGetValue()
	{
		$status = $this->make(\app\components\status\UserStatus::class, [
			'status' => \app\components\status\UserStatus::STATUS_ACTIVE,
		]);

		$this->assertEquals($status->getValue(), \app\components\status\UserStatus::STATUS_ACTIVE);
	}

	public function testGetTitle()
	{
		$status = $this->construct(\app\components\status\UserStatus::class, [
			'status' => \app\components\status\UserStatus::STATUS_ACTIVE,
			'translation' => new \app\components\translation\Translation()
		], [
		]);


		$this->assertEquals($status->getTitle(), 'Active');
	}

	public function testIncorrectValue()
	{
		try {
			$this->construct(\app\components\status\UserStatus::class, [
				'status' => 1000,
				'translation' => new \app\components\translation\Translation()
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
			$this->assertEquals($e->getMessage(), 'The status is incorrect');
		}
	}
}