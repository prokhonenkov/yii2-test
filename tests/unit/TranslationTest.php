<?php

class TranslationTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $translation;

    protected function _before()
    {
    	$this->translation = new \app\components\translation\Translation();
    }

    protected function _after()
    {
    }

    // tests
    public function testFull()
    {
		$object = $this->translation->setCategory('app');

		$this->assertTrue(get_class($object) === \app\components\translation\Translation::class);

		$object->setMessage('Test {param}');

		$this->assertTrue(get_class($object) === \app\components\translation\Translation::class);

		$object->setParams(['param' => 'as test']);

		$this->assertTrue(get_class($object) === \app\components\translation\Translation::class);

		$this->assertEquals($object->make(), 'Test as test');
    }
}