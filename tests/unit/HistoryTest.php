<?php

class HistoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

	public function _fixtures()
	{
		return [
			'history' => [
				'class' => \app\tests\fixtures\HistoryFixture::class,
				'dataFile' => codecept_data_dir() . 'History.php'
			],
			'task' => [
				'class' => \app\tests\fixtures\TaskFixture::class,
				'dataFile' => codecept_data_dir() . 'Task.php'
			],
			'sms' => [
				'class' => \app\tests\fixtures\SmsFixture::class,
				'dataFile' => codecept_data_dir() . 'Sms.php'
			],
			'call' => [
				'class' => \app\tests\fixtures\CallFixture::class,
				'dataFile' => codecept_data_dir() . 'Call.php'
			],
			'fax' => [
				'class' => \app\tests\fixtures\FaxFixture::class,
				'dataFile' => codecept_data_dir() . 'Fax.php'
			],
			'user' => [
				'class' => \app\tests\fixtures\UserFixture::class,
				'dataFile' => codecept_data_dir() . 'User.php'
			],
		];
	}
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
	private function testGetEvent()
    {
    	/** @var \app\models\History $history */
		$history = $this->tester->grabFixture('history', 'call');

		$this->assertTrue($history->getEvent() instanceof \app\components\event\EventInterface);
    }

	private   function testGetRelationTask()
	{
		/** @var \app\models\History $history */
		$history = $this->tester->grabFixture('history', 'task');
		$this->assertTrue($history->getRelationTask() instanceof \app\models\interfaces\TaskInterface);

		$history = $this->tester->grabFixture('history', 'empty');
		$this->assertNull($history->getRelationTask());
	}


	private function testGetRelationSms()
	{
		/** @var \app\models\History $history */
		$history = $this->tester->grabFixture('history', 'sms');
		$this->assertTrue($history->getRelationSms() instanceof \app\models\interfaces\SmsInterface);

		$history = $this->tester->grabFixture('history', 'empty');
		$this->assertNull($history->getRelationSms());
	}


	private  function testGetRelationCall()
	{
		/** @var \app\models\History $history */
		$history = $this->tester->grabFixture('history', 'call');
		$this->assertTrue($history->getRelationCall() instanceof \app\models\interfaces\CallInterface);

		$history = $this->tester->grabFixture('history', 'empty');
		$this->assertNull($history->getRelationCall());
	}

	private  function testGetRelationFax()
	{
		/** @var \app\models\History $history */
		$history = $this->tester->grabFixture('history', 'fax');
		$this->assertTrue($history->getRelationFax() instanceof \app\models\interfaces\FaxInterface);

		$history = $this->tester->grabFixture('history', 'empty');
		$this->assertNull($history->getRelationFax());
	}

	private function testGetSender()
	{

		$history = $this->tester->grabFixture('history', 'task');
		$this->assertTrue($history->getSender() instanceof \app\models\interfaces\UserInterface);

		$history = $this->tester->grabFixture('history', 'empty');
		$this->assertNull($history->getSender());
	}

	private function testGetDate()
	{
		/** @var \app\models\History $history */
		$history = $this->tester->grabFixture('history', 'task');

		$this->assertTrue(is_string($history->getDate()));
	}

	private function getDetail()
	{
		/** @var \app\models\History $history */
		$history = $this->tester->grabFixture('history', 'task');

		$this->assertTrue($history->getDetail() instanceof \app\components\historydetail\HistoryDetailInterface);
	}
}