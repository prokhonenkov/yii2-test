<?php

class FaxTypeTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $map;
    
    protected function _before()
    {
    	$this->map = [
			\app\components\faxtype\FaxType::TYPE_POA_ATC => 'Test 1',
		];
    }

    protected function _after()
    {
    }

    // tests
    public function testGetName()
    {
		$faxType = $this->make(\app\components\faxtype\FaxType::class, [
			'entityName' => 'test-1',
		]);

		$this->assertEquals($faxType->getName(), 'test-1');
    }

    public function testGetTitle()
	{
		$faxType = $this->construct(\app\components\faxtype\FaxType::class, [
			'entityName' => \app\components\faxtype\FaxType::TYPE_POA_ATC,
			'translation' => new \app\components\translation\Translation()
		], [
			'names' => $this->map,
		]);


		$this->assertEquals($faxType->getTitle(), 'Test 1');
	}

	public function testIncorrectValue()
	{
		try {
			$faxType = $this->construct(\app\components\faxtype\FaxType::class, [
				'entityName' => 'incorrect',
				'translation' => new \app\components\translation\Translation()
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
			$this->assertEquals($e->getMessage(), 'The fax type is incorrect');
		}
	}
}