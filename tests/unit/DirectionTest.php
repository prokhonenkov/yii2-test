<?php

use app\components\direction\Direction;

class DirectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private array $namesList;
    
    protected function _before()
    {
		$this->namesList = [
			Direction::DIRECTION_INCOMING => 'Incoming',
			Direction::DIRECTION_OUTGOING => 'Outgoing'
		];
    }

    protected function _after()
    {
    }

    // tests
    public function testIncoming()
    {
    	$direction = $this->make(Direction::class, [
			'directionId' => Direction::DIRECTION_INCOMING,
		]);

		$this->assertTrue($direction->isIncoming());
		$this->assertFalse($direction->isOutgoing());
	}

	public function testOutgoing()
    {
		$direction = $this->make(Direction::class, [
			'directionId' => Direction::DIRECTION_OUTGOING,
		]);

		$this->assertTrue($direction->isOutgoing());
		$this->assertFalse($direction->isIncoming());
	}

	public function testIncorrectDirrection()
    {
    	try {
			$direction = $this->construct(Direction::class, [
				'directionId' => 1000,
				'names' => $this->namesList
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
    		$this->assertEquals('$directionId is incorrect', $e->getMessage());
		}
	}

	public function testIncrrectListNames()
    {
    	try {
			$direction = $this->construct(Direction::class, [
				'directionId' => Direction::DIRECTION_OUTGOING,
				'names' => [
					1000 => 'Incoming',
					Direction::DIRECTION_OUTGOING => 'Outgoing'
				]
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
    		$this->assertEquals('DIRECTION_INCOMING not found', $e->getMessage());
		}

		try {
			$direction = $this->construct(Direction::class, [
				'directionId' => Direction::DIRECTION_OUTGOING,
				'names' => [
					Direction::DIRECTION_INCOMING => 'Outgoing',
					1000 => 'Incoming',
				]
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
    		$this->assertEquals('DIRECTION_OUTGOING not found', $e->getMessage());
		}
	}

	public function testGetName()
	{
		$direction = $this->make(Direction::class, [
			'directionId' => Direction::DIRECTION_OUTGOING,
			'names' => $this->namesList
		]);

		$this->assertTrue($direction->getName() === 'Outgoing');

		$direction = new Direction(Direction::DIRECTION_INCOMING, $this->namesList);

		$this->assertTrue($direction->getName() === 'Incoming');
	}
}