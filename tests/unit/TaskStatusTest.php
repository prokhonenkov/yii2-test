<?php

class TaskStatusTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

	protected function _before()
	{
	}

	protected function _after()
	{
	}

	// tests
	public function testGetValue()
	{
		$status = $this->make(\app\components\status\TaskStatus::class, [
			'status' => \app\components\status\TaskStatus::STATUS_NEW,
			'date' => date('Y-m-d')
		]);

		$this->assertEquals($status->getValue(), \app\components\status\TaskStatus::STATUS_NEW);
	}

	public function testGetTitle()
	{
		$status = $this->construct(\app\components\status\TaskStatus::class, [
			'status' => \app\components\status\TaskStatus::STATUS_NEW,
			'date' => date('Y-m-d'),
			'translation' => new \app\components\translation\Translation()
		], [
		]);


		$this->assertEquals($status->getTitle(), 'New');
	}

	public function testIncorrectName()
	{
		try {
			$this->construct(\app\components\status\TaskStatus::class, [
				'status' => 1000,
				'date' => date('Y-m-d'),
				'translation' => new \app\components\translation\Translation()
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
			$this->assertEquals($e->getMessage(), 'The status is incorrect');
		}
	}

	public function testIsDone()
	{
		$status = $this->make(\app\components\status\TaskStatus::class, [
			'status' => \app\components\status\TaskStatus::STATUS_DONE,
			'date' => date('Y-m-d')
		]);

		$this->assertTrue($status->isDone());
	}

	public function testIsOverdue()
	{
		$status = $this->make(\app\components\status\TaskStatus::class, [
			'status' => \app\components\status\TaskStatus::STATUS_DONE,
			'date' => date('Y-m-d')
		]);

		$this->assertTrue($status->isOverdue());
	}
}