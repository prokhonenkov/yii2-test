<?php

class BodyBuilderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testRenderAsHtml()
    {
    	/** @var \app\components\historylist\interfaces\BodyBuilderInterface $builder */
		$builder = $this->make(\app\components\historylist\body\BodyBuilder::class, [
			'view' => new class extends \yii\web\View {
				public function render($view, $params = [], $context = null)
				{
					return $view . '-' . implode(',', $params);
				}
			},
			'renderAsText' => false
		]);

		$this->assertTrue($builder->setViewPath('view') instanceof \app\components\historylist\interfaces\BodyBuilderInterface);
		$this->checkBuild($builder);
		$this->assertEquals($builder->render(), 'view-comment,data,text,user');
    }

    public function testRenderAsText()
    {
    	/** @var \app\components\historylist\interfaces\BodyBuilderInterface $builder */
		$builder = $this->construct(\app\components\historylist\body\BodyBuilder::class, [
			'view' => new class extends \yii\web\View {
				public function render($view, $params = [], $context = null)
				{
					return $view . '-' . implode(',', $params);
				}
			},
			'renderAsText' => true
		]);

		$this->checkBuild($builder);
		$this->assertEquals($builder->render(), 'comment data text user');
    }


    private function checkBuild($builder)
	{
		$user = $this->makeEmpty(\app\models\User::class, [
			'getName' => function(){
				return 'user';
			}
		]);

		$this->assertTrue($builder->setComment('comment') instanceof \app\components\historylist\interfaces\BodyBuilderInterface);
		$this->assertTrue($builder->setDate('data') instanceof \app\components\historylist\interfaces\BodyBuilderInterface);
		$this->assertTrue($builder->setText('text') instanceof \app\components\historylist\interfaces\BodyBuilderInterface);
		$this->assertTrue($builder->setUser($user) instanceof \app\components\historylist\interfaces\BodyBuilderInterface);
	}
}