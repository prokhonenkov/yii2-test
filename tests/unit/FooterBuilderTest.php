<?php

class FooterBuilderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testRenderAsHtml()
    {
    	/** @var \app\components\historylist\footer\FooterBuilder $builder */
		$builder = $this->make(\app\components\historylist\footer\FooterBuilder::class, [
			'view' => new class extends \yii\web\View {
				public function render($view, $params = [], $context = null)
				{
					return $view . '-' . implode(',', $params);
				}
			},
			'renderAsText' => false
		]);

		$this->assertTrue($builder->setViewPath('view') instanceof \app\components\historylist\interfaces\FooterBuilderInterface);
		$this->checkBuild($builder);
		$this->assertEquals($builder->render(), 'view-data,text');
    }

    public function testRenderAsText()
    {
		/** @var \app\components\historylist\footer\FooterBuilder $builder */
		$builder = $this->construct(\app\components\historylist\footer\FooterBuilder::class, [
			'view' => new class extends \yii\web\View {
				public function render($view, $params = [], $context = null)
				{
					return $view . '-' . implode(',', $params);
				}
			},
			'renderAsText' => true
		]);

		$this->checkBuild($builder);
		$this->assertEquals($builder->render(), 'data text');
    }


    private function checkBuild($builder)
	{
		$this->assertTrue($builder->setDate('data') instanceof \app\components\historylist\interfaces\FooterBuilderInterface);
		$this->assertTrue($builder->setText('text') instanceof \app\components\historylist\interfaces\FooterBuilderInterface);
	}
}