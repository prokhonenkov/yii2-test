<?php

class EventTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $eventMap;
    
    protected function _before()
    {
    	$this->eventMap = [
			\app\components\event\Event::EVENT_CREATED_TASK => 'Test 1',
			\app\components\event\Event::EVENT_UPDATED_TASK => 'Test 2'
		];
    }

    protected function _after()
    {
    }

    // tests
    public function testGetName()
    {
    	/** @var \app\components\event\Event $event */
		$event = $this->make(\app\components\event\Event::class, [
			'entityName' => 'test-1',
			'eventsMap' => $this->eventMap,
		]);

		$this->assertEquals($event->getName(), 'test-1');
    }

    public function testGetTitle()
	{
		/** @var \app\components\event\Event $event */
		$event = $this->construct(\app\components\event\Event::class, [
			'entityName' => \app\components\event\Event::EVENT_CREATED_TASK,
			'translation' => new \app\components\translation\Translation()
		], [
			'eventsMap' => $this->eventMap,
		]);


		$this->assertEquals($event->getTitle(), 'Test 1');
	}

	public function testIncorrectName()
	{
		/** @var \app\components\event\Event $event */
		try {
			$event = $this->construct(\app\components\event\Event::class, [
				'entityName' => 'incorrect',
				'translation' => new \app\components\translation\Translation()
			], [
				'eventsMap' => $this->eventMap,
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
			$this->assertEquals($e->getMessage(), 'The event is incorrect');
		}
	}
}