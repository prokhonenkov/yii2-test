<?php

class HistoryListRegistryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $registry;
    
    protected function _before()
    {
    	$this->registry = \app\components\historylist\HistoryListRegistry::getInstance();
    }

    protected function _after()
    {
    }

    // tests
    public function testGetHistoryTextFactoryFunctionMap()
    {
		$this->assertTrue(is_array($this->registry->getHistoryTextFactoryFunctionMap()));
    }

    public function testGetHistoryIconFactoryFunctionMap()
    {
		$this->assertTrue(is_array($this->registry->getHistoryIconFactoryFunctionMap()));
    }

    public function testGetHistoryCommentFactoryFunctionMap()
    {
		$this->assertTrue(is_array($this->registry->getHistoryCommentFactoryFunctionMap()));
    }

    public function testGetHistoryFooterTextFactoryFunctionMap()
    {
		$this->assertTrue(is_array($this->registry->getHistoryFooterTextFactoryFunctionMap()));
    }


    public function testGetHistoryMessageFooterTemplatesMap()
    {
		$this->assertTrue(is_array($this->registry->getHistoryMessageFooterTemplatesMap()));
    }

    public function testGetHistoryMessageTemplatesMap()
    {
		$this->assertTrue(is_array($this->registry->getHistoryMessageTemplatesMap()));
    }
}