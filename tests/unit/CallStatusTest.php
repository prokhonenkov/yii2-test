<?php

class CallStatusTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

	protected function _before()
	{
	}

	protected function _after()
	{
	}

	// tests
	public function testGetValue()
	{
		$status = $this->make(\app\components\status\CallStatus::class, [
			'status' => \app\components\status\CallStatus::STATUS_ANSWERED,
		]);

		$this->assertEquals($status->getValue(), \app\components\status\CallStatus::STATUS_ANSWERED);
	}

	public function testGetTitle()
	{
		$status = $this->construct(\app\components\status\CallStatus::class, [
			'status' => \app\components\status\CallStatus::STATUS_ANSWERED,
			'translation' => new \app\components\translation\Translation()
		], [
		]);


		$this->assertEquals($status->getTitle(), 'Answered');
	}

	public function testIncorrectValue()
	{
		try {
			$this->construct(\app\components\status\CallStatus::class, [
				'status' => 1000,
				'translation' => new \app\components\translation\Translation()
			]);

			$this->assertTrue(false);
		} catch (\Exception $e) {
			$this->assertEquals($e->getMessage(), 'The status is incorrect');
		}
	}

	public function testIsAnswered()
	{
		$status = $this->make(\app\components\status\CallStatus::class, [
			'status' => \app\components\status\CallStatus::STATUS_ANSWERED,
		]);

		$this->assertTrue($status->isAnswered());
	}
}