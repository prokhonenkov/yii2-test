<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 16.03.2020
 * Time 14:45
 */

namespace app\tests\fixtures;

use app\models\Sms;
use yii\test\ActiveFixture;

class SmsFixture extends ActiveFixture
{
	public $modelClass = Sms::class;
}