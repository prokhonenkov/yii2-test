<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 16.03.2020
 * Time 14:45
 */

namespace app\tests\fixtures;

use app\models\History;
use yii\test\ActiveFixture;

class HistoryFixture extends ActiveFixture
{
	public $modelClass = History::class;
	public $depends = [
		'app\tests\fixtures\TaskFixture',
		'app\tests\fixtures\SmsFixture',
		'app\tests\fixtures\FaxFixture',
		'app\tests\fixtures\CallFixture',
		'app\tests\fixtures\UserFixture',
	];
}