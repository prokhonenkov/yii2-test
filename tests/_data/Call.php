<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 16.03.2020
 * Time 15:59
 */

return [
	[
	'id' => '93756',
	'ins_ts' => '2017-09-14 18:04:56',
	'direction' => '0',
	'user_id' => '4040',
	'customer_id' => '7970',
	'status' => '1',
	'phone_from' => '4035',
	'phone_to' => '+6367112217704',
	'comment' => NULL
	],
	[
		'id' => '37266',
		'ins_ts' => '2017-09-27 22:28:30',
		'direction' => '0',
		'user_id' => '4034',
		'customer_id' => '7970',
		'status' => '1',
		'phone_from' => '4362',
		'phone_to' => '+6367112217704',
		'comment' => NULL
	],
	[
		'id' => '57645',
		'ins_ts' => '2017-10-05 18:36:45',
		'direction' => '0',
		'user_id' => NULL,'customer_id' => '7970',
		'status' => '0',
		'phone_from' => '+6075479762960',
		'phone_to' => '+6367112217704',
		'comment' => NULL
	],
	[
		'id' => '58406',
		'ins_ts' => '2017-10-05 20:20:28',
		'direction' => '0',
		'user_id' => '4040',
		'customer_id' => '7970',
		'status' => '1',
		'phone_from' => '4035',
		'phone_to' => '+6367112217704',
		'comment' => NULL
	],
	[
		'id' => '66070',
		'ins_ts' => '2017-10-10 20:53:35',
		'direction' => '0',
		'user_id' => NULL,'customer_id' => '7970',
		'status' => '1',
		'phone_from' => '+2651598436945',
		'phone_to' => '+6367112217704',
		'comment' => NULL
	]
];