<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 16.03.2020
 * Time 15:46
 */

return [
	[
		'id' => '5661',
		'user_id' => '4356',
		'customer_id' => '7550',
		'status' => '0',
		'phone_from' => '+1134685727065',
		'phone_to' => '+9563967002838',
		'message' => 'Aut accusamus fugiat hic neque reprehenderit consequatur expedita. Ut aspernatur quisquam ut provident dolores dolor qui. Tempore et fugiat a. A nihil est dolorem molestias debitis repellat quia.',
		'direction' => '1'
	],
	[
		'id' => '5662',
		'user_id' => '4356',
		'customer_id' => '7550',
		'status' => '0',
		'phone_from' => '+9214425742728',
		'phone_to' => '+9563967002838',
		'message' => 'Molestiae nihil aperiam et rerum. Ipsum debitis aut architecto aut ullam ut illo. Ut dolorem tempore ut est accusantium. Alias ab eligendi asperiores doloremque eius id.',
		'direction' => '0'
	],
	[
		'id' => '26941',
		'user_id' => '4450',
		'customer_id' => '7550',
		'status' => '0',
		'phone_from' => '9110',
		'phone_to' => '+9563967002838',
		'message' => 'Debitis facere dolores dolor molestiae dolore. Aut tenetur numquam veritatis magnam neque placeat. Repudiandae est dolor a quia et. Iusto asperiores rerum et eum voluptas repudiandae.',
		'direction' => '1'
	],
	[
		'id' => '26956',
		'user_id' => '4450',
		'customer_id' => '7550',
		'status' => '0',
		'phone_from' => '9110',
		'phone_to' => '+9563967002838',
		'message' => 'Voluptas dolores et tenetur velit. Tenetur excepturi dolores minima ea atque sit. Est tempore eligendi sint et.',
		'direction' => '1'],
	[
		'id' => '26961',
		'user_id' => '4450',
		'customer_id' => '7550',
		'status' => '0',
		'phone_from' => '9110',
		'phone_to' => '+9563967002838',
		'message' => 'Sunt voluptatem quibusdam aut culpa sit. Eligendi assumenda corrupti rem qui ut aut. Occaecati ut dignissimos cumque omnis molestiae quo commodi. Voluptas mollitia autem eum.',
		'direction' => '1'
	]
];