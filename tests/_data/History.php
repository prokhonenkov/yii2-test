<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 16.03.2020
 * Time 14:46
 */

return [
	'call' => [
		'id' => '788977',
		'ins_ts' => '2017-10-18 22:00:32',
		'customer_id' => '7550',
		'user_id' => 4034,
		'event' => 'incoming_call',
		'object' => 'call',
		'object_id' => '93756',
		'message' => 'message',
		'detail' => NULL
	],
	'fax' => [
		'id' => '11070861',
		'ins_ts' => '2019-02-28 15:18:26',
		'customer_id' => '413060',
		'user_id' => 4036,
		'event' => 'outgoing_fax',
		'object' => 'fax',
		'object_id' => '36051',
		'message' => 'message',
		'detail' => NULL
	],
	'sms' => [
		'id' => '876449',
		'ins_ts' => '2017-11-04 16:25:46',
		'customer_id' => '7550',
		'user_id' => '4356',
		'event' => 'outgoing_sms',
		'object' => 'sms',
		'object_id' => '5661',
		'message' => NULL,
		'detail' => NULL],
	'task' => [
		'id' => '2567099',
		'ins_ts' => '2018-02-13 23:27:36',
		'customer_id' => '7550',
		'user_id' => '4036',
		'event' => 'completed_task',
		'object' => 'task',
		'object_id' => '334228',
		'message' => 'task',
		'detail' => '{"changedAttributes":{"status":{"old":0,"new":1}}}'
	],
	'empty' => [
		'id' => '1',
		'ins_ts' => '2018-02-13 23:27:36',
		'customer_id' => '7550',
		'user_id' => null,
		'event' => 'completed_task',
		'object' => null,
		'object_id' => null,
		'message' => NULL,
		'detail' => '{"changedAttributes":{"status":{"old":0,"new":1}}}']
];