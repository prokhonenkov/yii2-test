<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 16.03.2020
 * Time 15:03
 */

return [

	[
		'id' => '334228',
		'user_id' => '4450',
		'customer_id' => '7970',
		'status' => '1',
		'title' => 'Missed call from Jordyn Collins',
		'text' => NULL,'due_date' => '2018-02-17 16:24:37',
		'priority' => NULL],
	[
		'id' => '338968',
		'user_id' => '4450',
		'customer_id' => '7970',
		'status' => '1',
		'title' => 'New sms from Lavonne Huel',
		'text' => NULL,'due_date' => '2018-02-17 16:52:46',
		'priority' => NULL],
	[
		'id' => '370762',
		'user_id' => '4503',
		'customer_id' => '7550',
		'status' => '1',
		'title' => 'New sms from Collin Mosciski',
		'text' => NULL,'due_date' => '2018-03-19 15:39:03',
		'priority' => NULL],
	[
		'id' => '388163',
		'user_id' => '4448',
		'customer_id' => '7970',
		'status' => '1',
		'title' => 'Missed call from Bert Lehner',
		'text' => NULL,'due_date' => '2018-04-02 13:17:16',
		'priority' => NULL],
	[
		'id' => '425075',
		'user_id' => '4503',
		'customer_id' => '7550',
		'status' => '1',
		'title' => 'New sms from Wilburn Schumm',
		'text' => NULL,'due_date' => '2018-04-30 12:41:47',
		'priority' => NULL]
];