<?php
namespace app\widgets\Export;


use kartik\export\ExportMenu;

class Export extends ExportMenu
{
    public $exportType = self::FORMAT_CSV;

    public function init()
    {
        if (empty($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        if (empty($this->exportRequestParam)) {
            $this->exportRequestParam = 'exportFull_' . $this->options['id'];
        }

        \Yii::$app->request->setBodyParams([
			$this->exportRequestParam => true,
			$this->exportTypeParam => $this->exportType,
			$this->colSelFlagParam => false
		]);


        parent::init();
    }
}