<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 13:14
 */

namespace app\widgets\HistoryList\components\interfaces;


interface FooterBuilderInterface extends BaseBuilderInterface
{
	/**
	 * @param string|null $text
	 * @return $this
	 */
	public function setText(?string $text): self ;

	/**
	 * @param string|null $date
	 * @return $this
	 */
	public function setDate(?string $date): self ;
}