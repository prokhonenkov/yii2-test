<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 14:49
 */

namespace app\widgets\HistoryList\components\interfaces;


use app\models\interfaces\UserInterface;

interface MessageInterface
{
	/**
	 * @return string|null
	 */
	public function getText(): ?string ;

	/**
	 * @return string|null
	 */
	public function getDate(): ?string ;

	/**
	 * @return UserInterface|null
	 */
	public function getAuthor(): ?UserInterface ;

	/**
	 * @return string|null
	 */
	public function getComment(): ?string ;

	/**
	 * @return string|null
	 */
	public function getFooterText(): ?string ;

	/**
	 * @return string|null
	 */
	public function getIconClass(): ?string ;

	/**
	 * @return string
	 */
	public function getMessageType(): string ;

	/**
	 * @return string
	 */
	public function getBodyType(): string ;

	/**
	 * @return string
	 */
	public function getFooterType(): string ;
}