<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 13:50
 */

namespace app\widgets\HistoryList\components\interfaces;


interface BaseBuilderInterface
{
	/**
	 * @param string $path
	 * @return $this
	 */
	public function setViewPath(string $path): self;

	/**
	 * @return string
	 */
	public function render(): string ;
}