<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 18:36
 */

namespace app\widgets\HistoryList\components\interfaces;


interface BodyDirectorInterface
{
	/**
	 * @return string
	 */
	public function getBody(): string ;
}