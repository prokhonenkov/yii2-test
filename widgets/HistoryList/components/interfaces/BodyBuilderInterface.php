<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 18:24
 */

namespace app\widgets\HistoryList\components\interfaces;


use app\models\interfaces\UserInterface;

interface BodyBuilderInterface extends BaseBuilderInterface
{
	public function setText(string $text): self ;

	public function setDate(string $date): self ;

	public function setUser(?UserInterface $user): self ;

	public function setComment(?string $comment): self ;
}