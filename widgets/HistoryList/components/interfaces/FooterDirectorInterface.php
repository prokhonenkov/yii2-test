<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 14:07
 */

namespace app\widgets\HistoryList\components\interfaces;


interface FooterDirectorInterface
{
	/**
	 * @return string
	 */
	public function getFooter(): string ;
}