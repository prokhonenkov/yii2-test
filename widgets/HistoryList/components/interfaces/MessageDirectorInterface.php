<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 18:58
 */

namespace app\widgets\HistoryList\components\interfaces;


interface MessageDirectorInterface
{
	/**
	 * @return string
	 */
	public function getMessage(): string ;
}