<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 19:25
 */

namespace app\widgets\HistoryList\components\interfaces;


interface EventRenderInterface
{
	/**
	 * @return string|null
	 */
	public function getType(): ?string ;

	/**
	 * @return string|null
	 */
	public function getTitle(): ?string ;

	/**
	 * @return string|null
	 */
	public function getBodyText(): ?string ;

	/**
	 * @return string|null
	 */
	public function getFooterText(): ?string ;

	/**
	 * @return string|null
	 */
	public function getComment(): ?string ;

	/**
	 * @return string|null
	 */
	public function getIcon(): ?string ;

	/**
	 * @return string
	 */
	public function getMessageTemplate(): string ;

	/**
	 * @return string
	 */
	public function getMessageBodyTemplate(): string ;

	/**
	 * @return string
	 */
	public function getMessageFooterTemplate(): string ;

}