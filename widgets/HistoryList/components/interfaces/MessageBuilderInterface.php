<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 15:19
 */

namespace app\widgets\HistoryList\components\interfaces;


use app\models\interfaces\UserInterface;

interface MessageBuilderInterface extends BaseBuilderInterface
{
	/**
	 * @param string $text
	 * @return $this
	 */
	public function setBody(string $text): self ;

	/**
	 * @param string|null $text
	 * @return $this
	 */
	public function setFooter(?string $text): self ;

	/**
	 * @param string $class
	 * @return $this
	 */
	public function setIconClass(string $class): self ;
}