<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 18:24
 */

namespace  app\widgets\HistoryList\components\body;


use app\widgets\HistoryList\components\abstracts\BuilderAbstracts;
use app\widgets\HistoryList\components\interfaces\BodyBuilderInterface;
use app\models\interfaces\UserInterface;

class BodyBuilder extends BuilderAbstracts implements BodyBuilderInterface
{
	/**
	 * @param string|null $comment
	 * @return BodyBuilderInterface
	 */
	public function setComment(?string $comment): BodyBuilderInterface
	{
		$this->data['comment'] = $comment;

		return $this;
	}

	/**
	 * @param string|null $date
	 * @return BodyBuilderInterface
	 */
	public function setDate(?string $date): BodyBuilderInterface
	{
		$this->data['date'] = $date;

		return $this;
	}

	/**
	 * @param string|null $date
	 * @return BodyBuilderInterface
	 */
	public function setText(?string $date): BodyBuilderInterface
	{
		$this->data['text'] = $date;

		return $this;
	}

	/**
	 * @param UserInterface|null $user
	 * @return BodyBuilderInterface
	 */
	public function setUser(?UserInterface $user): BodyBuilderInterface
	{
		if(!$user) {
			$this->data['user'] = null;
		} else {
			$this->data['user'] = $user->getName();
		}

		return $this;
	}

}