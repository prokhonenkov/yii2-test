<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 18:32
 */

namespace app\widgets\HistoryList\components\body;


use app\widgets\HistoryList\components\interfaces\BodyBuilderInterface;
use app\widgets\HistoryList\components\interfaces\BodyDirectorInterface;
use app\widgets\HistoryList\components\interfaces\MessageInterface;

class BodyDirector implements BodyDirectorInterface
{
	/**
	 * @var BodyBuilderInterface
	 */
	private BodyBuilderInterface $builder;
	/**
	 * @var MessageInterface
	 */
	private MessageInterface $message;

	/**
	 * BodyDirector constructor.
	 * @param BodyBuilderInterface $builder
	 * @param MessageInterface $message
	 */
	public function __construct(BodyBuilderInterface $builder, MessageInterface $message)
	{
		$this->builder = clone $builder;
		$this->message = $message;
	}

	/**
	 * @return string
	 */
	private function getBodyDefault(): string
	{
		return $this->builder
			->setViewPath('@app/widgets/HistoryList/views/_body')
			->setText($this->message->getText())
			->setUser($this->message->getAuthor())
			->setComment($this->message->getComment())
			->render();
	}

	/**
	 * @return string
	 */
	private function getBodyWithDate(): string
	{
		return $this->builder
			->setViewPath('@app/widgets/HistoryList/views/_body')
			->setDate($this->message->getDate())
			->setText($this->message->getText())
			->setUser($this->message->getAuthor())
			->setComment($this->message->getComment())
			->render();
	}

	/**
	 * @return string
	 */
	public function getBody(): string
	{
		$method = sprintf('getBody%s',
			ucfirst($this->message->getBodyType())
		);

		return $this->{$method}();
	}
}