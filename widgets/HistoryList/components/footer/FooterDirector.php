<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 14:04
 */

namespace app\widgets\HistoryList\components\footer;


use app\widgets\HistoryList\components\interfaces\FooterBuilderInterface;
use app\widgets\HistoryList\components\interfaces\FooterDirectorInterface;
use app\widgets\HistoryList\components\interfaces\MessageInterface;

class FooterDirector implements FooterDirectorInterface
{
	/**
	 * @var FooterBuilderInterface
	 */
	private FooterBuilderInterface $builder;
	/**
	 * @var MessageInterface
	 */
	private MessageInterface $message;

	/**
	 * FooterDirector constructor.
	 * @param FooterBuilderInterface $builder
	 * @param MessageInterface $message
	 */
	public function __construct(FooterBuilderInterface $builder, MessageInterface $message)
	{
		$this->builder = clone $builder;
		$this->message = $message;
	}

	/**
	 * @return string
	 */
	private function getFooterWithDate()
	{
		return $this->builder
			->setViewPath('@app/widgets/HistoryList/views/_footer')
			->setDate($this->message->getDate())
			->setText($this->message->getFooterText())
			->render();
	}

	/**
	 * @return string
	 */
	private function getFooterWithoutDate()
	{
		return $this->builder
			->setViewPath('@app/widgets/HistoryList/views/_footer')
			->setText($this->message->getFooterText())
			->render();
	}

	/**
	 * @return string
	 */
	public function getFooter(): string
	{
		$method = sprintf('getFooter%s',
			ucfirst($this->message->getFooterType())
		);

		return $this->{$method}();
	}
}