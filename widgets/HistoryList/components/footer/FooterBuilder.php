<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 13:13
 */

namespace  app\widgets\HistoryList\components\footer;


use app\widgets\HistoryList\components\abstracts\BuilderAbstracts;
use app\widgets\HistoryList\components\interfaces\FooterBuilderInterface;

class FooterBuilder extends BuilderAbstracts implements FooterBuilderInterface
{
	/**
	 * @param string|null $date
	 * @return FooterBuilderInterface
	 */
	public function setDate(?string $date): FooterBuilderInterface
	{
		$this->data['date'] = $date;

		return $this;
	}

	/**
	 * @param string|null $text
	 * @return FooterBuilderInterface
	 */
	public function setText(?string $text): FooterBuilderInterface
	{
		$this->data['text'] = $text;

		return $this;
	}
}