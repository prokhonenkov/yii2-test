<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 13:48
 */

namespace  app\widgets\HistoryList\components\abstracts;


use app\widgets\HistoryList\components\interfaces\BaseBuilderInterface;
use yii\web\View;

abstract class BuilderAbstracts implements BaseBuilderInterface
{
	/**
	 * @var View
	 */
	protected View $view;
	/**
	 * @var string
	 */
	protected string $viewPath;
	/**
	 * @var array
	 */
	protected array $data = [];
	/**
	 * @var bool
	 */
	private bool $renderAsText = false;

	/**
	 * BuilderAbstracts constructor.
	 * @param View $view
	 * @param bool $renderAsText
	 */
	public function __construct(View $view, bool $renderAsText = false)
	{
		$this->view = $view;
		$this->renderAsText = $renderAsText;
	}

	/**
	 * @param string $path
	 * @return $this
	 */
	public function setViewPath(string $path): self
	{
		$this->viewPath = $path;

		return $this;
	}

	/**
	 * @return string
	 */
	public function render(): string
	{
		if($this->renderAsText) {
			return strip_tags(implode(' ', $this->data));
		}

		return $this->view->render($this->viewPath, $this->data);
	}
}