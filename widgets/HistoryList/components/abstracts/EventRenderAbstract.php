<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 19:25
 */

namespace  app\widgets\HistoryList\components\abstracts;

use app\components\translation\TranslationInterface;
use app\models\interfaces\HistoryInterface;
use app\widgets\HistoryList\components\interfaces\EventRenderInterface;

abstract class EventRenderAbstract implements EventRenderInterface
{
	public const MESSAGE_TEMPLATE_COMMON = 'common';
	public const MESSAGE_TEMPLATE_STATUS = 'status';

	public const BODY_TEMPLATE_DEFAULT = 'default';
	public const BODY_TEMPLATE_WITH_DATE = 'withDate';

	public const FOOTER_TEMPLATE_WITH_DATE = 'withDate';
	public const FOOTER_TEMPLATE_WITHOUT_DATE = 'withoutDate';

	/**
	 * @var TranslationInterface
	 */
	protected TranslationInterface $translation;
	/**
	 * @var HistoryInterface
	 */
	protected HistoryInterface $event;

	/**
	 * EventRenderAbstract constructor.
	 * @param HistoryInterface $event
	 * @param TranslationInterface $translation
	 */
	public function __construct(HistoryInterface $event, TranslationInterface $translation)
	{
		$this->event = $event;
		$this->translation = $translation;
	}

	/**
	 * @return string|null
	 */
	public function getBodyText(): ?string
	{
		return null;
	}

	/**
	 * @return string|null
	 */
	public function getComment(): ?string
	{
		return null;
	}

	/**
	 * @return string|null
	 */
	public function getIcon(): ?string
	{
		return 'fa-gear bg-purple-light';
	}

	/**
	 * @return string|null
	 */
	public function getFooterText(): ?string
	{
		return null;
	}

	/**
	 * @return string|null
	 */
	public function getType(): ?string
	{
		return $this->event->getEventType();
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->event->getRelationObject()->getEventTitleByName($this->event->getEventType());
	}

	/**
	 * @return string
	 */
	public function getMessageTemplate(): string
	{
		return self::MESSAGE_TEMPLATE_COMMON;
	}

	/**
	 * @return string
	 */
	public function getMessageBodyTemplate(): string
	{
		return self::BODY_TEMPLATE_DEFAULT;
	}

	/**
	 * @return string
	 */
	public function getMessageFooterTemplate(): string
	{
		return self::FOOTER_TEMPLATE_WITH_DATE;
	}
}