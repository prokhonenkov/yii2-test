<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 21.03.2020
 * Time 9:37
 */

namespace app\widgets\HistoryList\components\event;


use app\components\translation\TranslationInterface;
use app\models\interfaces\HistoryInterface;
use app\models\interfaces\SmsInterface;
use app\widgets\HistoryList\components\abstracts\EventRenderAbstract;

class SmsEvent extends EventRenderAbstract
{
	/**
	 * @var SmsInterface|null
	 */
	private SmsInterface $sms;

	/**
	 * SmsEvent constructor.
	 * @param HistoryInterface $event
	 * @param TranslationInterface $translation
	 */
	public function __construct(HistoryInterface $event, TranslationInterface $translation)
	{
		parent::__construct($event, $translation);
		$this->sms = $this->event->getRelationObject();
	}

	/**
	 * @return string|null
	 */
	public function getFooterText(): ?string
	{
		$direction = $this->sms->getDirection();

		if($direction->isIncoming()) {
			return $this->translation
				->setMessage('Incoming message from {number}')
				->setParams(['number' => $this->sms->getPhoneFrom()])
				->make();
		}

		return $this->translation
			->setMessage('Sent message to {number}')
			->setParams(['number' => $this->sms->getPhoneTo()])
			->make();
	}

	/**
	 * @return string|null
	 */
	public function getIcon(): ?string
	{
		return 'fa-check-square bg-yellow';
	}

	/**
	 * @return string|null
	 */
	public function getBodyText(): ?string
	{
		return $this->sms->getMessage();
	}
}