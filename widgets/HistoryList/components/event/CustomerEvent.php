<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 21.03.2020
 * Time 9:48
 */

namespace app\widgets\HistoryList\components\event;


use app\widgets\HistoryList\components\abstracts\EventRenderAbstract;
use app\widgets\HistoryList\components\event\type\CustomerTypeAbstract;
use app\components\translation\TranslationInterface;
use app\models\interfaces\HistoryInterface;

class CustomerEvent extends EventRenderAbstract
{
	/**
	 * @var CustomerTypeAbstract
	 */
	private CustomerTypeAbstract $type;

	/**
	 * CustomerEvent constructor.
	 * @param HistoryInterface $event
	 * @param TranslationInterface $translation
	 */
	public function __construct(HistoryInterface $event, TranslationInterface $translation)
	{
		parent::__construct($event, $translation);

		$className = 'app\widgets\HistoryList\components\event\type\\' . $this->getTypeClass();
		$this->type = new $className($this->event);
	}

	/**
	 * @return string|null
	 */
	public function getBodyText(): ?string
	{
		return $this->type->render();
	}

	/**
	 * @return string
	 */
	public function getMessageTemplate(): string
	{
		return self::MESSAGE_TEMPLATE_STATUS;
	}

	/**
	 * @return string
	 */
	public function getMessageBodyTemplate(): string
	{
		return self::BODY_TEMPLATE_WITH_DATE;
	}

	/**
	 * @return string
	 */
	public function getMessageFooterTemplate(): string
	{
		return self::FOOTER_TEMPLATE_WITHOUT_DATE;
	}

	/**
	 * @return string
	 */
	private function getTypeClass()
	{
		return implode('', array_map(function(string $item) {
			return ucfirst($item);
		}, explode('_', $this->event->getEventType())));
	}
}