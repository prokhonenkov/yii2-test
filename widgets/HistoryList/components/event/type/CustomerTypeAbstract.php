<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 19:43
 */

namespace app\widgets\HistoryList\components\event\type;


use app\models\interfaces\HistoryInterface;
use yii\helpers\Html;

abstract class CustomerTypeAbstract
{
	/**
	 * @var HistoryInterface
	 */
	protected HistoryInterface $historyModel;

	/**
	 * @return string
	 */
	abstract protected function getOldValue(): string ;

	/**
	 * @return string
	 */
	abstract protected function getNewValue(): string ;

	/**
	 * CustomerTypeAbstract constructor.
	 * @param HistoryInterface $historyModel
	 */
	public function __construct(HistoryInterface $historyModel)
	{
		$this->historyModel = $historyModel;
	}

	/**
	 * @return string|null
	 */
	public function render(): ?string
	{
		return sprintf('%s %s &#8594; %s',
			$this->historyModel->getRelationObject()->getEventTitleByName($this->historyModel->getEventType()),
			$this->renderOldValue(),
			$this->renderNewValue()
		);
	}

	/**
	 * @return string
	 */
	private function renderNewValue(): string
	{
		return Html::tag('span', $this->getNewValue(), ['class' => 'badge badge-pill badge-success']);
	}

	/**
	 * @return string
	 */
	protected function renderOldValue(): string
	{
		return Html::tag('span', $this->getOldValue(), ['class' => 'badge badge-pill badge-warning']);
	}
}