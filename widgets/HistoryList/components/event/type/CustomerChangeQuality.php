<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 18:46
 */

namespace app\widgets\HistoryList\components\event\type;


use yii\helpers\Html;

class CustomerChangeQuality extends CustomerTypeAbstract
{
	/**
	 * @return string
	 */
	protected function getOldValue(): string
	{
		$oldValue = $this->historyModel
			->getDetail()
			->getQuality()
			->getOldValue();

		$value = $oldValue->getTitle();

		if($oldValue->isDefault()) {
			$value = Html::tag('i', $value);
		}

		return $value;
	}

	/**
	 * @return string
	 */
	protected function getNewValue(): string
	{
		$newValue = $this->historyModel
			->getDetail()
			->getQuality()
			->getNewValue();

		$value = $newValue->getTitle();

		if($newValue->isDefault()) {
			$value = Html::tag('i', $value);
		}

		return $value;
	}
}