<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 19:42
 */

namespace app\widgets\HistoryList\components\event\type;


use yii\helpers\Html;

class CustomerChangeType extends CustomerTypeAbstract
{
	/**
	 * @return string
	 */
	protected function getOldValue(): string
	{
		$oldValue = $this->historyModel
			->getDetail()
			->getType()
			->getOldValue();

		$value = $oldValue->getTitle();

		if($oldValue->isDefault()) {
			$value = Html::tag('i', $value);
		}

		return $value;
	}

	/**
	 * @return string
	 */
	protected function getNewValue(): string
	{
		$newValue = $this->historyModel
			->getDetail()
			->getType()
			->getNewValue();

		$value = $newValue->getTitle();

		if($newValue->isDefault()) {
			$value = Html::tag('i', $value);
		}

		return $value;
	}
}