<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 21.03.2020
 * Time 9:10
 */

namespace app\widgets\HistoryList\components\event;


use app\components\translation\TranslationInterface;
use app\models\interfaces\FaxInterface;
use app\models\interfaces\HistoryInterface;
use app\widgets\HistoryList\components\abstracts\EventRenderAbstract;

class FaxEvent extends EventRenderAbstract
{
	/**
	 * @var FaxInterface|null
	 */
	private FaxInterface $fax;

	/**
	 * FaxEvent constructor.
	 * @param HistoryInterface $event
	 * @param TranslationInterface $translation
	 */
	public function __construct(HistoryInterface $event, TranslationInterface $translation)
	{
		parent::__construct($event, $translation);
		$this->fax = $this->event->getRelationObject();
	}

	/**
	 * @return string|null
	 */
	public function getFooterText(): ?string
	{
		if(!$this->fax->getType()) {
			return null;
		}

		return sprintf('%s %s',
			$this->fax->getType()->getTitle(),
			$this->translation
				->setMessage('was sent')
				->make()
		);
	}

	/**
	 * @return string|null
	 */
	public function getIcon(): ?string
	{
		return 'fa-fax bg-green';
	}

	/**
	 * @return string|null
	 */
	public function getBodyText(): ?string
	{
		return $this->getTitle();
	}
}