<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 21.03.2020
 * Time 18:44
 */

namespace app\widgets\HistoryList\components\event;


use app\widgets\HistoryList\components\abstracts\EventRenderAbstract;

class DefaultEvent extends EventRenderAbstract
{
	/**
	 * @return array
	 */
	protected function getEventTypeMap(): array
	{
		return [];
	}
}