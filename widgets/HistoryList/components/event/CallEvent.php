<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 21.03.2020
 * Time 8:14
 */

namespace app\widgets\HistoryList\components\event;


use app\components\translation\TranslationInterface;
use app\models\interfaces\CallInterface;
use app\models\interfaces\HistoryInterface;
use app\widgets\HistoryList\components\abstracts\EventRenderAbstract;
use yii\helpers\Html;

class CallEvent extends EventRenderAbstract
{
	/**
	 * @var CallInterface|null
	 */
	private CallInterface $call;

	/**
	 * CallEvent constructor.
	 * @param HistoryInterface $event
	 * @param TranslationInterface $translation
	 */
	public function __construct(HistoryInterface $event, TranslationInterface $translation)
	{
		parent::__construct($event, $translation);

		$this->call = $this->event->getRelationObject();
	}

	/**
	 * @return string
	 */
	public function getBodyText(): string
	{
		if(!$this->call) {
			return Html::tag('i', 'Deleted');
		}

		$comment = $this->call->getComment();

		if($comment) {
			$comment = Html::tag('span', $comment, ['class' => 'text-grey']);
		}

		return sprintf('%s %s',
			$this->call->getInfo(),
			$comment
		);
	}

	/**
	 * @return string
	 */
	public function getFooterText(): ?string
	{
		return null;
	}

	/**
	 * @return string
	 */
	public function getComment(): ?string
	{
		return $this->call->getComment();
	}

	/**
	 * @return string
	 */
	public function getIcon(): string
	{
		if(!$this->call || !$this->call->getStatus()->isAnswered()) {
			return 'md-phone-missed bg-red';
		}

		return 'md-phone bg-green';
	}
}