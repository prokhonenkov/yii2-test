<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 21.03.2020
 * Time 9:40
 */

namespace app\widgets\HistoryList\components\event;


use app\components\translation\TranslationInterface;
use app\models\interfaces\HistoryInterface;
use app\models\interfaces\TaskInterface;
use app\widgets\HistoryList\components\abstracts\EventRenderAbstract;

class TaskEvent extends EventRenderAbstract
{
	/**
	 * @var TaskInterface|null
	 */
	private ?TaskInterface $task;

	/**
	 * TaskEvent constructor.
	 * @param HistoryInterface $event
	 * @param TranslationInterface $translation
	 */
	public function __construct(HistoryInterface $event, TranslationInterface $translation)
	{
		parent::__construct($event, $translation);
		$this->task = $this->event->getRelationObject();
	}

	/**
	 * @return string|null
	 */
	public function getFooterText(): ?string
	{
		if(!$this->task) {
			return null;
		}

		$creditor = $this->task->getCreditor();
		if(!$creditor) {
			return null;
		}

		if(!$creditor->getName()) {
			return null;
		}

		return $creditor->getName();
	}

	/**
	 * @return string|null
	 */
	public function getIcon(): ?string
	{
		return 'fa-check-square bg-yellow';
	}

	/**
	 * @return string|null
	 */
	public function getBodyText(): ?string
	{
		$title = null;
		if($this->task) {
			$title = $this->task->getTitle();
		}
		return sprintf('%s %s',
			$this->getTitle(),
			$title
		);
	}
}