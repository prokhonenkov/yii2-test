<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 20:42
 */

namespace app\widgets\HistoryList\components\message;


use app\components\translation\Translation;
use app\widgets\HistoryList\components\interfaces\EventRenderInterface;
use app\widgets\HistoryList\components\interfaces\MessageInterface;
use app\models\interfaces\HistoryInterface;
use app\models\interfaces\UserInterface;
use app\widgets\DateTime\DateTime;
use yii\base\InvalidConfigException;

class HistoryMessage implements MessageInterface
{
	/**
	 * @var HistoryInterface
	 */
	private HistoryInterface $model;

	/**
	 * @var EventRenderInterface
	 */
	private $event;

	/**
	 * HistoryMessage constructor.
	 * @param HistoryInterface $model
	 */
	public function __construct(HistoryInterface $model)
	{
		$this->model = $model;

		$relationClass = $this->model->getRelationObject();

		$eventClassName = sprintf('app\widgets\HistoryList\components\event\%sEvent',
			$relationClass
				? (new \ReflectionClass($this->model->getRelationObject()))->getShortName()
				: 'Default'
		);

		$this->event = new $eventClassName($this->model, new Translation());
	}

	/**
	 * @return string|null
	 */
	public function getText(): ?string
	{
		return $this->event->getBodyText();
	}

	/**
	 * @return UserInterface|null
	 */
	public function getAuthor(): ?UserInterface
	{
		return $this->model->getSender();
	}

	/**
	 * @return string|null
	 * @throws \Exception
	 */
	public function getDate(): ?string
	{
		return DateTime::widget([
			'dateTime' => $this->model->getDate()
		]);
	}

	/**
	 * @return string|null
	 */
	public function getFooterText(): ?string
	{
		return $this->event->getFooterText();
	}

	/**
	 * @return string|null
	 */
	public function getComment(): ?string
	{
		return $this->event->getComment();
	}

	/**
	 * @return string|null
	 */
	public function getIconClass(): ?string
	{
		return $this->event->getIcon();
	}

	/**
	 * @return string
	 */
	public function getMessageType(): string
	{
		return $this->event->getMessageTemplate();
	}

	/**
	 * @return string
	 */
	public function getBodyType(): string
	{
		return $this->event->getMessageBodyTemplate();
	}

	/**
	 * @return string
	 */
	public function getFooterType(): string
	{
		return $this->event->getMessageFooterTemplate();
	}
}