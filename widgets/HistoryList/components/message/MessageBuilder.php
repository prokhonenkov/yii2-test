<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 19:21
 */

namespace app\widgets\HistoryList\components\message;


use app\widgets\HistoryList\components\abstracts\BuilderAbstracts;
use app\widgets\HistoryList\components\interfaces\MessageBuilderInterface;

class MessageBuilder extends BuilderAbstracts implements MessageBuilderInterface
{
	/**
	 * @param string $text
	 * @return MessageBuilderInterface
	 */
	public function setBody(string $text): MessageBuilderInterface
	{
		$this->data['body'] = $text;

		return $this;
	}

	/**
	 * @param string $class
	 * @return MessageBuilderInterface
	 */
	public function setIconClass(string $class): MessageBuilderInterface
	{
		$this->data['iconClass'] = $class;

		return $this;
	}

	/**
	 * @param string|null $text
	 * @return MessageBuilderInterface
	 */
	public function setFooter(?string $text): MessageBuilderInterface
	{
		$this->data['footer'] = $text;

		return $this;
	}
}