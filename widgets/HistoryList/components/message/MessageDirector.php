<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 19:01
 */

namespace app\widgets\HistoryList\components\message;


use app\widgets\HistoryList\components\interfaces\BodyDirectorInterface;
use app\widgets\HistoryList\components\interfaces\FooterDirectorInterface;
use app\widgets\HistoryList\components\interfaces\MessageBuilderInterface;
use app\widgets\HistoryList\components\interfaces\MessageDirectorInterface;
use app\widgets\HistoryList\components\interfaces\MessageInterface;

class MessageDirector implements MessageDirectorInterface
{
	/**
	 * @var MessageBuilderInterface
	 */
	private MessageBuilderInterface $builder;
	/**
	 * @var MessageInterface
	 */
	private MessageInterface $message;
	/**
	 * @var BodyDirectorInterface
	 */
	private BodyDirectorInterface $bodyBuilderDirector;
	/**
	 * @var FooterDirectorInterface
	 */
	private FooterDirectorInterface $footerBuilderDirector;/**

	/**
	 * MessageDirector constructor.
	 * @param MessageBuilderInterface $builder
	 * @param BodyDirectorInterface $bodyBuilderDirector
	 * @param FooterDirectorInterface $footerBuilderDirector
	 * @param MessageInterface $message
	 */
	public function __construct(
		MessageBuilderInterface $builder,
		BodyDirectorInterface $bodyBuilderDirector,
		FooterDirectorInterface $footerBuilderDirector,
		MessageInterface $message
	)
	{
		$this->builder = clone $builder;
		$this->message = $message;
		$this->bodyBuilderDirector = $bodyBuilderDirector;
		$this->footerBuilderDirector = $footerBuilderDirector;
	}

	/**
	 * @return string
	 */
	private function getCommonMessage(): string
	{
		return $this->builder
			->setViewPath('@app/widgets/HistoryList/views/_item_common')
			->setBody($this->bodyBuilderDirector->getBody())
			->setIconClass($this->message->getIconClass())
			->setFooter($this->footerBuilderDirector->getFooter())
			->render();
	}

	/**
	 * @return string
	 */
	private function getStatusMessage(): string
	{
		return $this->builder
			->setViewPath('@app/widgets/HistoryList/views/_item_status')
			->setBody($this->bodyBuilderDirector->getBody())
			->render();
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		$method = sprintf('get%sMessage',
			$this->message->getMessageType()
		);

		return $this->{$method}();
	}
}