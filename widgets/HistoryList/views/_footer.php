<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 12.03.2020
 * Time 20:23
 */

/** @var string $text */
/** @var string $date */
?>

<div class="bg-warning">
	<?= $text ?>
	<?php if (!empty($date)): ?>
		<span><?= $date; ?></span>
	<?php endif; ?>
</div>
