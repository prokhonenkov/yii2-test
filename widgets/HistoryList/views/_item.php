<?php

use \app\widgets\HistoryList\components\message\MessageDirector;
use \app\widgets\HistoryList\components\message\MessageBuilder;
use \app\widgets\HistoryList\components\message\HistoryMessage;
use \app\widgets\HistoryList\components\body\BodyDirector;
use \app\widgets\HistoryList\components\body\BodyBuilder;
use \app\widgets\HistoryList\components\footer\FooterDirector;
use \app\widgets\HistoryList\components\footer\FooterBuilder;

/** @var $model \app\models\interfaces\HistoryInterface */

$message = new HistoryMessage($model);

echo (
	new MessageDirector(
		new MessageBuilder($this),
		new BodyDirector(new BodyBuilder($this), $message),
		new FooterDirector(new FooterBuilder($this), $message),
		$message
    )
)->getMessage();

?>
