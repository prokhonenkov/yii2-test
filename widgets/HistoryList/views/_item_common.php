<?php
use yii\helpers\Html;

/* @var string $body */
/* @var string $footer */
/* @var string $iconClass */
?>

<?= Html::tag(
        'i',
        '',
        ['class' => "icon icon-circle icon-main white $iconClass"]
); ?>

<?= $body; ?>

<?= $footer; ?>