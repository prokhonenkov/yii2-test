<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 18:00
 */

/** @var string $text */
/** @var string $date */
/** @var string $user */
/** @var string $comment */
?>

<div class="bg-success ">
	<?= $text; ?>

	<?php if (!empty($date)): ?>
        <span><?= $date; ?></span>
	<?php endif; ?>
</div>

<?php if ($user): ?>
    <div class="bg-info"><?= $user; ?></div>
<?php endif; ?>

<?php if ($comment): ?>
    <div class="bg-info"><?= $comment ?></div>
<?php endif; ?>
