<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $model \app\models\search\HistorySearch */
/* @var $linkExport string */

?>

<div class="panel panel-primary panel-small m-b-0">
    <div class="panel-body panel-body-selected">

        <div class="pull-sm-right">
			<?php if (!empty($linkExport)): ?>
				<?= Html::a(Yii::t('app', 'CSV'), $linkExport, [
					'class' => 'btn btn-success',
					'data-pjax' => 0
				]); ?>
			<?php endif; ?>
        </div>

    </div>
</div>

<?php Pjax::begin(['id' => 'grid-pjax', 'formSelector' => false]); ?>

<?= ListView::widget([
	'dataProvider' => $dataProvider,
	'itemView' => '_item',
	'options' => [
		'tag' => 'ul',
		'class' => 'list-group'
	],
	'itemOptions' => [
		'tag' => 'li',
		'class' => 'list-group-item'
	],
	'emptyTextOptions' => ['class' => 'empty p-20'],
	'layout' => '{items}{pager}',
]); ?>

<?php Pjax::end(); ?>
