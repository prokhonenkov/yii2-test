<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 17:37
 */

namespace app\components\historydetail;


class Value implements ValueInterface
{
	/**
	 * @var mixed
	 */
	private $value;
	/**
	 * @var string
	 */
	private string $title;
	/**
	 * @var bool
	 */
	private bool $isDefaultValue = false;

	/**
	 * Value constructor.
	 * @param $value
	 * @param string $title
	 * @param bool $isDefaultValue
	 */
	public function __construct($value, string $title, bool $isDefaultValue = false)
	{
		$this->value = $value;
		$this->title = $title;
		$this->isDefaultValue = $isDefaultValue;
	}

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return bool
	 */
	public function isDefault(): bool
	{
		return $this->isDefaultValue;
	}
}