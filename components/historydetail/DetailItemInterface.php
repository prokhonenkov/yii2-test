<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 19:19
 */

namespace app\components\historydetail;


interface DetailItemInterface
{
	/**
	 * @return ValueInterface
	 */
	public function getOldValue(): ValueInterface ;

	/**
	 * @return ValueInterface
	 */
	public function getNewValue(): ValueInterface ;
}