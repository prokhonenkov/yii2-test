<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 18:32
 */

namespace app\components\historydetail;


interface HistoryDetailInterface
{
	/**
	 * @return DetailItemInterface|null
	 */
	public function getQuality(): ?DetailItemInterface ;

	/**
	 * @return DetailItemInterface|null
	 */
	public function getType(): ?DetailItemInterface ;
}