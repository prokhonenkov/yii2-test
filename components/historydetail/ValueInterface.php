<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 17:23
 */

namespace app\components\historydetail;


interface ValueInterface
{
	/**
	 * @return string
	 */
	public function getTitle(): string ;

	/**
	 * @return mixed
	 */
	public function getValue();

	/**
	 * @return bool
	 */
	public function isDefault():bool ;
}