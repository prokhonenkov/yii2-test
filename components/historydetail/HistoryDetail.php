<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 16:49
 */

namespace app\components\historydetail;


use app\components\historydetail\items\Quality;
use app\components\historydetail\items\Type;
use app\components\translation\Translation;

class HistoryDetail implements HistoryDetailInterface
{
	/**
	 * @var array|mixed
	 */
	private array $detail = [];

	/**
	 * HistoryDetail constructor.
	 * @param string $json
	 */
	public function __construct(string $json)
	{
		try {
			$this->detail = json_decode($json, true);
		} catch (\Exception $e) {

		}
	}

	/**
	 * @return DetailItemInterface|null
	 */
	public function getQuality(): ?DetailItemInterface
	{
		if(!$this->getByKey('quality')) {
			return null;
		}

		return new Quality($this->getByKey('quality'), new Translation());
	}

	/**
	 * @return DetailItemInterface|null
	 */
	public function getType(): ?DetailItemInterface
	{
		if(!$this->getByKey('type')) {
			return null;
		}

		return new Type($this->getByKey('type'), new Translation());
	}

	/**
	 * @param string $key
	 * @return array|null
	 */
	private function getByKey(string $key): ?array
	{
		return $this->detail['changedAttributes'][$key] ?? null;
	}
}