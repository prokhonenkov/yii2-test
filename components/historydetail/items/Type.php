<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 19:22
 */

namespace app\components\historydetail\items;


final class Type extends ItemAbstract
{
	public const TYPE_LEAD = 'lead';
	public const TYPE_DEAL = 'deal';
	public const TYPE_LOAN = 'loan';

	/**
	 * @return array
	 */
	protected function getDataMap(): array
	{
		return [
			self::TYPE_LEAD => 'Lead',
			self::TYPE_DEAL => 'Deal',
			self::TYPE_LOAN => 'Loan',
		];
	}
}