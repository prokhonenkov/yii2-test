<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 16:39
 */

namespace app\components\historydetail\items;


final class Quality extends ItemAbstract
{
	public const QUALITY_ACTIVE = 'active';
	public const QUALITY_REJECTED = 'rejected';
	public const QUALITY_COMMUNITY = 'community';
	public const QUALITY_UNASSIGNED = 'unassigned';
	public const QUALITY_TRICKLE = 'trickle';

	/**
	 * @return array
	 */
	protected function getDataMap(): array
	{
		return [
			self::QUALITY_ACTIVE => 'Active',
			self::QUALITY_REJECTED => 'Rejected',
			self::QUALITY_COMMUNITY => 'Community',
			self::QUALITY_UNASSIGNED => 'Unassigned',
			self::QUALITY_TRICKLE => 'Trickle',
		];
	}
}