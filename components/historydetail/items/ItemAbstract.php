<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 19:26
 */

namespace app\components\historydetail\items;


use app\components\historydetail\DetailItemInterface;
use app\components\historydetail\Value;
use app\components\historydetail\ValueInterface;
use app\components\translation\TranslationInterface;

abstract class ItemAbstract implements DetailItemInterface
{
	public const NO_SET = 'not-set';
	/**
	 * @var array
	 */
	protected array $data;
	/**
	 * @var TranslationInterface
	 */
	private TranslationInterface $translation;

	/**
	 * @var array
	 */
	private array $dataMap = [
		self::NO_SET => 'not set'
	];

	/**
	 * @return array
	 */
	abstract protected function getDataMap(): array ;

	/**
	 * ItemAbstract constructor.
	 * @param array $data
	 */
	public function __construct(array $data, TranslationInterface $translation)
	{
		$this->data = $data;
		$this->dataMap = array_merge($this->dataMap, $this->getDataMap());
		$this->translation = $translation;
	}

	/**
	 * @return ValueInterface
	 */
	public function getNewValue(): ValueInterface
	{
		return $this->getValue('new');
	}

	/**
	 * @return ValueInterface
	 */
	public function getOldValue(): ValueInterface
	{
		return $this->getValue('old');
	}

	/**
	 * @param string $value
	 * @return string
	 */
	protected function translate(string $value): string
	{
		return $this->translation
			->setMessage($value)
			->make();
	}

	/**
	 * @param string $key
	 * @return ValueInterface
	 */
	protected function getValue(string $key): ValueInterface
	{
		$isDefault = false;
		$value = $this->data[$key] ?? null;

		if(!$value) {
			$value = self::NO_SET;
			$isDefault = true;
		}

		if(!empty($this->dataMap[$value])){
			$title = $this->translate($this->dataMap[$value]);
		} else {
			$title = $value;
		}

		return new Value(
			$value,
			$title,
			$isDefault
		);
	}
}