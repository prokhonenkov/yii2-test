<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 10:56
 */

namespace app\components\translation;


interface TranslationInterface
{
	/**
	 * @param string $category
	 * @return $this
	 */
	public function setCategory(string $category): self ;

	/**
	 * @param string $message
	 * @return $this
	 */
	public function setMessage(string $message): self ;

	/**
	 * @param array $params
	 * @return $this
	 */
	public function setParams(array $params): self ;

	/**
	 * @return string
	 */
	public function make(): string ;
}