<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 10:58
 */

namespace app\components\translation;


class Translation implements TranslationInterface
{
	/**
	 * @var string
	 */
	private string $message;
	/**
	 * @var array
	 */
	private array $params = [];
	/**
	 * @var string
	 */
	private string $category = 'app';

	/**
	 * @param string $category
	 * @return TranslationInterface
	 */
	public function setCategory(string $category): TranslationInterface
	{
		$this->category = $category;

		return $this;
	}

	/**
	 * @param string $message
	 * @return TranslationInterface
	 */
	public function setMessage(string $message): TranslationInterface
	{
		$this->message = $message;

		return $this;
	}

	/**
	 * @param array $params
	 * @return TranslationInterface
	 */
	public function setParams(array $params): TranslationInterface
	{
		$this->params = $params;

		return $this;
	}

	/**
	 * @return string
	 */
	public function make(): string
	{
		return \Yii::t($this->category, $this->message, $this->params);
	}
}