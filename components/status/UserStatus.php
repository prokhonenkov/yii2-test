<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 10:05
 */

namespace app\components\status;


use app\components\status\interfaces\UserStatusInterface;

class UserStatus extends StatusAbstract implements UserStatusInterface
{
	public const STATUS_DELETED = 0;
	public const STATUS_HIDDEN = 1;
	public const STATUS_ACTIVE = 10;

	/**
	 * @return array
	 */
	protected function getStatusMap(): array
	{
		return  [
			self::STATUS_ACTIVE => 'Active',
			self::STATUS_DELETED => 'Deleted',
			self::STATUS_HIDDEN => 'Hidden',
		];
	}

	/**
	 * @return array
	 */
	public static function getList(): array
	{
		return [
			self::STATUS_ACTIVE,
			self::STATUS_HIDDEN,
			self::STATUS_DELETED
		];
	}
}