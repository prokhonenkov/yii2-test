<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 9:52
 */

namespace app\components\status;


use app\components\status\interfaces\TaskStatusInterface;
use app\components\translation\TranslationInterface;

class TaskStatus extends StatusAbstract implements TaskStatusInterface
{
	public const STATUS_NEW = 0;
	public const STATUS_DONE = 1;
	public const STATUS_CANCEL = 3;
	/**
	 * @var string|null
	 */
	private ?string $date = null;

	/**
	 * TaskStatus constructor.
	 * @param int $status
	 * @param string $date
	 * @param TranslationInterface $translation
	 */
	public function __construct(int $status, string $date, TranslationInterface $translation)
	{
		parent::__construct($status, $translation);

		$this->date = $date;
	}

	/**
	 * @return array
	 */
	protected function getStatusMap(): array
	{
		return  [
			self::STATUS_NEW => 'New',
			self::STATUS_DONE => 'Complete',
			self::STATUS_CANCEL => 'Cancel',
		];
	}

	/**
	 * @return bool
	 */
	public function isDone(): bool
	{
		return $this->status === self::STATUS_DONE;
	}

	/**
	 * @return bool
	 */
	public function isOverdue(): bool
	{
		return $this->isDone() && strtotime($this->date) < time();
	}
}