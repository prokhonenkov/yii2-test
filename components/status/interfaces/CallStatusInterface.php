<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 11:09
 */

namespace app\components\status\interfaces;


interface CallStatusInterface extends StatusInterface
{
	/**
	 * @return bool
	 */
	public function isAnswered(): bool ;
}