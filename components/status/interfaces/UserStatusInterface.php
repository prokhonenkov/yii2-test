<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 10:05
 */

namespace app\components\status\interfaces;


interface UserStatusInterface extends StatusInterface
{
	/**
	 * @return array
	 */
	public static function getList(): array ;
}