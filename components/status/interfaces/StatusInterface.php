<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 9:23
 */

namespace app\components\status\interfaces;


interface StatusInterface
{
	/**
	 * @return int
	 */
	public function getValue(): int;

	/**
	 * @return string
	 */
	public function getTitle(): string ;
}