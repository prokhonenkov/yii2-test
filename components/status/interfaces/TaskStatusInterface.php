<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 9:50
 */

namespace app\components\status\interfaces;


interface TaskStatusInterface extends StatusInterface
{
	/**
	 * @return bool
	 */
	public function isDone(): bool ;

	/**
	 * @return bool
	 */
	public function isOverdue(): bool ;
}