<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 8:59
 */

namespace app\components\status;


use app\components\status\interfaces\StatusInterface;
use app\components\translation\TranslationInterface;
use yii\base\InvalidArgumentException;

abstract class StatusAbstract implements StatusInterface
{
	/**
	 * @var int
	 */
	protected int $status;
	/**
	 * @var TranslationInterface
	 */
	private TranslationInterface $translation;

	abstract protected function getStatusMap(): array ;

	/**
	 * StatusAbstract constructor.
	 * @param int $status
	 * @param TranslationInterface $translation
	 */
	public function __construct(int $status, TranslationInterface $translation)
	{
		$map = $this->getStatusMap();
		if(!isset($map[$status])) {
			throw new InvalidArgumentException('The status is incorrect');
		}
		$this->status = $status;
		$this->translation = $translation;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		$map = $this->getStatusMap();
		return $this->translation
			->setMessage($map[$this->status])
			->make();
	}

	/**
	 * @return int
	 */
	public function getValue(): int
	{
		return $this->status;
	}
}