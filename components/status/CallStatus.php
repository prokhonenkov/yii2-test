<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 14.03.2020
 * Time 11:10
 */

namespace app\components\status;


use app\components\status\interfaces\CallStatusInterface;

class CallStatus extends StatusAbstract implements CallStatusInterface
{
	public const STATUS_NO_ANSWERED = 0;
	public const STATUS_ANSWERED = 1;

	/**
	 * @return bool
	 */
	public function isAnswered(): bool
	{
		return $this->status === self::STATUS_ANSWERED;
	}

	/**
	 * @return array
	 */
	protected function getStatusMap(): array
	{
		return [
			self::STATUS_ANSWERED => 'Answered',
			self::STATUS_NO_ANSWERED => 'No answered'
		];
	}

}