<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 9:28
 */

namespace app\components\status;


use app\components\status\interfaces\SmsStatusInterface;

class SmsStatus extends StatusAbstract implements SmsStatusInterface
{
	public const STATUS_NEW = 0;
	public const STATUS_READ = 1;
	public const STATUS_ANSWERED = 2;

	// outgoing
	public const STATUS_DRAFT = 10;
	public const STATUS_WAIT = 11;
	public const STATUS_SENT = 12;
	public const STATUS_DELIVERED = 13;
	public const STATUS_FAILED = 14;
	public const STATUS_SUCCESS = 15;

	/**
	 * @return array
	 */
	protected function getStatusMap(): array
	{
		return [
			self::STATUS_NEW => 'New',
			self::STATUS_READ => 'Read',
			self::STATUS_ANSWERED => 'Answered',
			self::STATUS_DRAFT => 'Draft',
			self::STATUS_WAIT => 'Wait',
			self::STATUS_SENT => 'Sent',
			self::STATUS_DELIVERED => 'Delivered',
			self::STATUS_FAILED => 'Failed',
			self::STATUS_SUCCESS => 'Success',
		];
	}
}