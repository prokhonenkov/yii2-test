<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 20:37
 */

namespace app\components\entity;


use app\components\translation\TranslationInterface;

abstract class EntityAbstract implements EntityInterface
{
	/**
	 * @var string
	 */
	protected string $entityName;
	/**
	 * @var TranslationInterface
	 */
	private TranslationInterface $translation;

	/**
	 * @return array
	 */
	abstract protected function getEntityMap(): array ;

	/**
	 * EntityAbstract constructor.
	 * @param string $entityName
	 * @param TranslationInterface $translation
	 */
	public function __construct(string $entityName, TranslationInterface $translation)
	{
		$this->entityName = $entityName;
		$this->translation = $translation;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		$map = $this->getEntityMap();

		return $this->translation
			->setMessage($map[$this->entityName])
			->make();
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->entityName;
	}
}