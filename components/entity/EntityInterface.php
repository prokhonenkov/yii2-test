<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 15.03.2020
 * Time 20:34
 */

namespace app\components\entity;


interface EntityInterface
{
	/**
	 * @return string
	 */
	public function getName(): string ;

	/**
	 * @return string
	 */
	public function getTitle(): string ;
}