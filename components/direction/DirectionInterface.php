<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 10:21
 */

namespace app\components\direction;


use app\components\entity\EntityInterface;

interface DirectionInterface extends EntityInterface
{
	/**
	 * @return bool
	 */
	public function isIncoming(): bool ;

	/**
	 * @return bool
	 */
	public function isOutgoing(): bool ;
}