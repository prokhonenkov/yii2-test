<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 10:19
 */

namespace app\components\direction;


use yii\base\InvalidArgumentException;

class Direction implements DirectionInterface
{
	public const DIRECTION_INCOMING = 0;
	public const DIRECTION_OUTGOING = 1;

	private array $list = [
		self::DIRECTION_OUTGOING,
		self::DIRECTION_INCOMING
	];

	private array $names = [];

	/**
	 * @var int
	 */
	private int $directionId;

	/**
	 * Direction constructor.
	 * @param int $directionId
	 * @param array $names
	 */
	public function __construct(int $directionId, array $names)
	{
		$this->directionId = $directionId;

		if(!$this->isExist()) {
			throw new InvalidArgumentException('$directionId is incorrect');
		}

		if(!isset($names[self::DIRECTION_INCOMING])) {
			throw new InvalidArgumentException('DIRECTION_INCOMING not found');
		}

		if(!isset($names[self::DIRECTION_OUTGOING])) {
			throw new InvalidArgumentException('DIRECTION_OUTGOING not found');
		}

		$this->names = $names;
	}


	/**
	 * @return bool
	 */
	public function isIncoming(): bool
	{
		return $this->directionId === self::DIRECTION_INCOMING;
	}

	/**
	 * @return bool
	 */
	public function isOutgoing(): bool
	{
		return $this->directionId === self::DIRECTION_OUTGOING;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->names[$this->directionId];
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->names[$this->directionId];
	}

	/**
	 * @return bool
	 */
	private function isExist(): bool
	{
		return in_array($this->directionId, $this->list);
	}
}