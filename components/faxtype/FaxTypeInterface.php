<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 11:32
 */

namespace app\components\faxtype;


use app\components\entity\EntityInterface;

interface FaxTypeInterface extends EntityInterface
{

}