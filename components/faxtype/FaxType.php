<?php
/**
 * Created by Vitaliy Prokhonenkov <prokhonenkov@gmail.com>
 * Date 13.03.2020
 * Time 11:38
 */

namespace app\components\faxtype;


use app\components\entity\EntityAbstract;
use app\components\translation\TranslationInterface;
use yii\base\InvalidArgumentException;

class FaxType extends EntityAbstract implements FaxTypeInterface
{
	public const TYPE_POA_ATC = 'poa_atc';
	public const TYPE_REVOCATION_NOTICE = 'revocation_notice';

	/**
	 * @var array
	 */
	private array $names = [
		self::TYPE_POA_ATC => 'POA/ATC',
		self::TYPE_REVOCATION_NOTICE => 'Revocation',
	];

	public function __construct(string $faxType, TranslationInterface $translation)
	{
		parent::__construct($faxType, $translation);

		if(!isset($this->names[$faxType])) {
			throw new InvalidArgumentException('The fax type is incorrect');
		}
	}

	protected function getEntityMap(): array
	{
		return $this->names;
	}
}