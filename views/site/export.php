<?php

/**
 * @var $this yii\web\View
 * @var $model \app\models\History
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $exportType string
 */

use app\models\History;
use app\widgets\Export\Export;
use \app\widgets\HistoryList\components\body\BodyDirector;
use \app\widgets\HistoryList\components\body\BodyBuilder;


?>

<?= Export::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'ins_ts',
            'label' => Yii::t('app', 'Date'),
            'format' => 'datetime'
        ],
        [
            'label' => Yii::t('app', 'User'),
            'value' => function (History $model) {
                return $model->getSender()
					? $model->getSender()->getName()
					: Yii::t('app', 'System');
            }
        ],
        [
            'label' => Yii::t('app', 'Type'),
            'value' => function (History $model) {
                return $model->object;
            }
        ],
        [
            'label' => Yii::t('app', 'Event'),
            'value' => function (History $model) {
				if(!$model->getRelationObject()) {
					return null;
				}
				return $model->getRelationObject()->getEventTitleByName($model->getEventType());
			}
        ],
        [
            'label' => Yii::t('app', 'Message'),
            'value' => function (History $model) {
                return (new BodyDirector(
					new BodyBuilder($this, true),
					new \app\widgets\HistoryList\components\message\HistoryMessage($model)
				))->getBody();
            }
        ]
    ],
    'exportType' => $exportType,
    'batchSize' => 2000,
    'filename' => 'history-' . time()
]);